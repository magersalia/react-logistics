<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\PickupDetail;
use App\DeliveryDetail;
use Illuminate\Support\Arr;

use App\ParcelTimeline;
class OrderController extends Controller
{
    //
    
    public function index(Request $request){

        $orders = Order::with(['pickup_details','delivery_details','status','rider']);
        
        if($request->has('sort')){
            if($request->sort == 'newest'){
                $orders->latest();
            }else{
                $orders->oldest();
            }
        }

        
        return response()->json(['message' => 'OK', 'data' => $orders->get()]);   
   }

   public function create(Request $request){    
    $order = new Order;

         
    $validatedData = $request->validate([
        'order_id' => ['required', 'unique:orders', 'max:255'],
        'tracking_no' => ['required'],
        'size' => ['required'],
        'weight' => ['required'],
        'total' => ['required'],
        'pickup_details.contact_person' => ['required'],
        'pickup_details.contact_no' => ['required'],
        'pickup_details.address' => ['required'],
        'delivery_details.contact_person' => ['required'],
        'delivery_details.contact_no' => ['required'],
        'delivery_details.address' => ['required'],
    ]);


    if(!$validatedData){
        return response()->json(['errors'=>$validator->errors()], 422);
    }

    
     
    $order->order_id = $request->order_id;
    $order->tracking_no = $request->tracking_no;
    $order->size = $request->size; 
    $order->weight = $request->weight; 
    $order->total = $request->total;
    $order->item = $request->item;
      
    $order->bentahan_order_id = $request->bentahan_order_id ?? null;  
    $order->save();
 

    if(request()->has("delivery_details")){
        $data = request()->get('delivery_details'); 
        $data['order_id'] = $order->id; 
        DeliveryDetail::create($data);
    }

    if(request()->has("pickup_details")){
        $data = request()->get('pickup_details');
        $data['order_id'] =  $order->id; 
        PickupDetail::create($data);
    }
      

        $this->saveTimeLine($order->id,'Created','Order has been created');


    return response()->json(['success' => true]);    
   }

   public function edit(Request $request){
        $order = Order::with(['pickup_details','delivery_details'])->whereId($request->id)->first();
        return response()->json(['message' => 'OK', 'data' => $order]);  
    } 
 
    public function update(Request $request){
        $allData = collect(request()->all())->except(['delivery_details','pickup_details']);
        $order = Order::find($request->id); 
        $order->update($allData->toArray());

        if(request()->has("delivery_details")){
            $data = request()->get('delivery_details');  
            DeliveryDetail::find($order->delivery_details->id)->update($data);
        }

        if(request()->has("pickup_details")){
            $data = request()->get('pickup_details'); 
            PickupDetail::find($order->pickup_details->id)->update($data);
        }
 
        return response()->json(['success' => true]);   
    }

    
    public function delete(Request $request){
        $order = Order::with(['pickup_details','delivery_details'])->whereId($request->id)->delete();
        return response()->json(['message' => 'OK']);  
    } 

    public function saveTimeLine($order_id,$status,$description){
        $data = new ParcelTimeline();
        $data->order_id = $order_id;
        $data->status = $status;
        $data->description = $description;
        $data->save();
    }

}
