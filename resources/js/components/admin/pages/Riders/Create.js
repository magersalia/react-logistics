import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
 
import CreateUpdateForm from './CreateUpdateForm';
import { PlusIcon,ArrowLeftIcon } from '@heroicons/react/solid'


function Index (props) {

  let [users,setUsers] = useState(null);
  let [loading,setLoading] = useState(false);
 
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
 

  
  useEffect(() => { 
    // getData() 
}, []);

 
  
  return ( 
      <div>
        {
          loading ?
            <div class="w-full h-full fixed block top-0 left-0 bg-gray-800 opacity-75">
              <span class="text-green-500 z-99 top-1/2 my-0 mx-auto block relative w-0 h-0"
              style={{top: '50%'}}>
              <div class="loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-10 w-10"></div>
              </span>
            </div>
            :
            ''
        }
        
        <header className="bg-white overflow-hidden  mb-4">
        <div className=" py-3 grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500 mt-2">
        <Link to='/admin/riders'>
        <ArrowLeftIcon className="inline h-6 w-6 pb-1 mr-4" area-hidden="true" />
        </Link>
          Register New Rider</h1>
        </div> 
        </div>
      </header>

        <CreateUpdateForm />
      
      </div>
  );
}

Index.propTypes = {}

export default Index;

