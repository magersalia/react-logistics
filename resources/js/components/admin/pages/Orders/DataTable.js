import React,{Fragment,useEffect, useState} from 'react';


import { Disclosure, Menu, Transition } from '@headlessui/react'
import { TrashIcon,EyeIcon,PencilIcon } from '@heroicons/react/solid'
import AssignRiderModal from './AssignRiderModal'; 
import { useHistory } from 'react-router';
import moment from 'moment';
import Avatar from 'react-avatar';
import Loader from '../../../rider/pages/Loader';

function DataTable (props) { 

  let [showRiderModal,setShowRiderModal] = useState(false);
  let [selectedOrder,setSelectedOrder] = useState({});
  let [ridersList,setRidersList] = useState(null);
  let [currentRider,setCurrentRider] = useState(null);

  const getData = () =>{
    let url = '/api/admin/riders'; 
    axios.get(url)
    .then(response => {  
        setRidersList(response.data.data)   
    })
    .catch(error => { 
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        } 
    });
  }
  const deleteRecord = (id) =>{
    let url = '/api/admin/orders/delete/'+id; 
    axios.get(url)
    .then(response => {  

      props.reloadData && props.reloadData()
      props.toast && props.toast('Data successfully deleted')

    })
    .catch(error => { 
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        } 
    }); 
    
  }

  
  
  useEffect(() => { 
    getData() 
    }, []);

  const statusColor = (status = 'Cancelled') =>{
    var color = '';
    switch (status) {
      case 'Cancelled':
        color = 'bg-red-500 text-red-100';
        break; 
        case 'Pending':
          color = 'bg-yellow-500 text-red-100';
          break; 
      default: 
          color = 'bg-green-500 text-green-100';
          break; 
    }

    return color;
  }
  
    const {heading,data} = props;  

    const toggleRiderModal = () =>{
      setShowRiderModal(!showRiderModal);
      setCurrentRider(null)
    }

   const saveSuccess = () =>{
    props.reloadData && props.reloadData()
    toggleRiderModal()
   }


    let history = useHistory(); 
    return ( 
        <div className="flex flex-col ">
          {
            showRiderModal ?
            <AssignRiderModal 
            open={showRiderModal}
            toggleModal={toggleRiderModal}
            orderData={selectedOrder}
            ridersList={ridersList}
            currentRider={currentRider}
            saveSuccess={saveSuccess}
            toast={props.toast}
            />
            :
            null
          } 
      <div className="-my-2 overflow-x-auto sm:-mx-6  ">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200 border border-gray-100"> 
            <thead className="bg-gray-100 border-b-2 border-t-1 border-gray-300">  
              <tr> 
                  <th>#</th>
      {
                    heading ?
                        heading.map((val)=> 
                        <th
                        scope="col"
                        className="px-2 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider"
                      >{val.name}</th>
                        ) 
                    : null
                } 
                </tr> 
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {data  ? data.map((item,key) => (
                  <tr key={key} className="hover:bg-gray-100 hover:border-b-8 cursor-pointer">
                    <td className="px-2 py-3 whitespace-nowrap text-sm text-gray-500  border-b" >{key + 1}</td>
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">
                    <div className="flex items-center">
                        <div className="flex-shrink-0">
                            <div className="text-sm font-medium text-gray-900">Item:</div>
                            <div className="text-sm text-gray-500">{item.item}</div>
                            <div className="text-sm font-medium text-gray-900">Order id:</div>
                            <div className="text-sm text-gray-500">{item.order_id}</div>
                            <div className="text-sm font-medium text-gray-900">Tracking no:</div>
                            <div className="text-sm text-gray-500">{item.tracking_no}</div>
                        </div>
                    </div>
                      </td>
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">{item.size}</td>
                     <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">{item.weight}</td>
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">{item.total}</td>
                    
                    <td className="px-2 py-3 text-sm border-b  align-middle" width="0">
                      <label className="font-medium text-gray-900 mr-2 mb-3">Pickup:</label><br></br>
                      <span className="text-gray-700">{item.pickup_details.address ?? 'No address set'}</span><br></br>
                      <label className="font-medium text-gray-900 mr-2">Deliver:</label><br></br>
                      <span className="text-gray-700">{item.delivery_details.address  ?? 'No address set'}</span>
                    </td> 
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b align-middle">
                    <div className="flex  ">
                        <div className="flex-shrink-0">
                            <div className="text-sm font-medium text-gray-900">Created at:</div>
                            <div className="text-sm text-gray-500">{moment(item.created_at).format("YYYY-MM-DD hh:mm a")}</div>
                            <div className="text-sm font-medium text-gray-900 ">Time Since Placed:</div>
                            <div className="text-sm text-gray-500">{item.time_since}</div>
                        </div>
                    </div>
                    </td>
                    {/* <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">{moment(item.created_at).format("YYYY/MM/DD hh:mm a")}</td>
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b">{item.time_since}</td> */}
                    <td className="px-2 py-3 whitespace-nowrap text-sm  border-b  align-middle" width="200">
                    <div className="flex items-center">
                        <div className="flex-shrink-0 h-10 w-10">
                        <Avatar  
                        className="border rounded-full border-gray-200"
                        src={item.rider.avatar}
                        name={item.rider.name}
                        size={40} round="100%" />
                         </div>
                        <div className="ml-4">
                          <div className="text-sm font-medium w-full text-gray-900">{item.rider.name}</div>
                          <div className="text-sm font-medium text-gray-900"><a onClick={(e)=>{
                            e.preventDefault()
                            setShowRiderModal(true)
                            setSelectedOrder(item)
                            setCurrentRider(item.rider_id)
                          }} className=" font-small text-blue-400 hover:text-blue-600  block underline">Edit</a></div>

                        </div>
                      </div>  
                    </td>
                    <td className="px-2 py-3 whitespace-nowrap text-sm  align-middle  border-b">
                      <span className={` px-2 py-1 text-xs  rounded-full   ${statusColor(item.status.name)}`}>
                      {item.status.name}
                      </span>
                    </td>
                    
                    <td className="px-2 py-3 whitespace-nowrap text-right  align-middle  border-b text-sm font-medium" width="120" >
                    <button onClick={()=>alert('HEY')} className={`bg-green-500 hover:bg-green-700 text-white  rounded py-2 px-2   mr-1`}>
                    <EyeIcon className="h-4 w-4" aria-hidden="true" /> 
                    </button>  
                    <button onClick={()=>
                    history.push(`/admin/orders/edit/${item.id}`)
                    } className={`bg-green-500 hover:bg-green-700 text-white   py-2 px-2 rounded mr-1`}>
                    <PencilIcon className="h-4 w-4" aria-hidden="true" /> 
                    </button>   
                    <button onClick={()=>{
                      deleteRecord(item.id)
                    }} className={`bg-red-600 hover:bg-red-700 text-white   py-2 px-2 rounded`}>
                    <TrashIcon className="h-4 w-4" aria-hidden="true" />
                    </button> 
                    </td>
                  </tr>
                )): 
                      <Loader /> 
                }
              </tbody>
            </table>
          </div>
          </div>
          </div>
        </div> 
    )
}


export default DataTable;