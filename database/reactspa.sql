-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2021 at 02:04 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reactspa`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_details`
--

CREATE TABLE `delivery_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `delivered_at` datetime DEFAULT NULL,
  `cancelled_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `isCancelled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_details`
--

INSERT INTO `delivery_details` (`id`, `order_id`, `contact_no`, `contact_person`, `address`, `created_at`, `updated_at`, `status`, `delivered_at`, `cancelled_reason`, `cancelled_at`, `isCancelled`) VALUES
(1, '1', '091123344556', 'Wenser Florendo', 'Pasig City', '2021-09-11 17:09:04', '2021-09-11 17:09:04', 1, NULL, NULL, NULL, 0),
(2, '2', '0911233445562312', 'Wenser Florendo', '123123', '2021-09-11 17:45:47', '2021-09-11 17:45:47', 1, NULL, NULL, NULL, 0),
(3, '3', '312312312', 'Wenser Florendo', '12312', '2021-09-11 18:16:54', '2021-09-11 18:19:25', 2, '2021-09-12 02:19:25', NULL, NULL, 0),
(4, '4', '09199817854', 'Wenser Florendo', '123123', '2021-09-11 18:21:31', '2021-09-11 18:24:02', 1, NULL, 'Wrong Address', '2021-09-12 02:24:02', 1),
(5, '5', '4456123', 'ASDw12l3asjkld', '1215123as5ASD', '2021-09-18 18:18:27', '2021-09-18 18:18:27', 1, NULL, NULL, NULL, 0),
(6, '6', '11111111111', 'BBBBBBBBBB', 'BBBBBBBBBBBBB BBBBBBBBB', '2021-09-18 20:29:03', '2021-09-18 20:29:03', 1, NULL, NULL, NULL, 0),
(7, '7', '248as4d', 'as54d8we', '654a8s7d89wq', '2021-09-18 20:45:09', '2021-09-18 20:45:09', 1, NULL, NULL, NULL, 0),
(8, '8', 'bbbbbbb', 'bbbbbbbbb', 'bbbbbbbbb', '2021-09-19 02:13:14', '2021-09-19 02:13:14', 1, NULL, NULL, NULL, 0),
(9, '11', 'customer no', 'Bentahan customer', 'customer address', '2021-09-27 18:08:04', '2021-09-27 18:08:04', 1, NULL, NULL, NULL, 0),
(10, '12', 'customer no', 'Bentahan customer', 'customer address', '2021-09-27 19:02:11', '2021-09-27 19:02:11', 1, NULL, NULL, NULL, 0),
(11, '13', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:04:05', '2021-09-27 19:04:05', 1, NULL, NULL, NULL, 0),
(12, '14', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:06:44', '2021-09-27 19:06:44', 1, NULL, NULL, NULL, 0),
(13, '15', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:08:16', '2021-09-27 19:08:16', 1, NULL, NULL, NULL, 0),
(14, '16', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:08:24', '2021-09-27 19:08:24', 1, NULL, NULL, NULL, 0),
(15, '17', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:09:46', '2021-09-27 19:09:46', 1, NULL, NULL, NULL, 0),
(16, '18', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:10:46', '2021-09-27 19:10:46', 1, NULL, NULL, NULL, 0),
(17, '19', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:32:57', '2021-09-27 19:32:57', 1, NULL, NULL, NULL, 0),
(18, '20', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-27 19:33:15', '2021-09-27 19:33:15', 1, NULL, NULL, NULL, 0),
(19, '21', '123123', '231231', 'sadasd', '2021-09-27 19:40:03', '2021-09-27 19:40:03', 1, NULL, NULL, NULL, 0),
(20, '22', '13123', '3123', 'asda', '2021-09-27 22:44:25', '2021-09-27 22:44:25', 1, NULL, NULL, NULL, 0),
(21, '23', '231', 'asdasd', '123123', '2021-09-27 22:45:02', '2021-09-27 22:45:02', 1, NULL, NULL, NULL, 0),
(22, '24', '3123', '312312', '32123123', '2021-09-27 22:47:50', '2021-09-27 22:47:50', 1, NULL, NULL, NULL, 0),
(23, '25', 'asd', 'asdasdasdasd', 'asdasd', '2021-09-27 23:15:19', '2021-09-27 23:15:19', 1, NULL, NULL, NULL, 0),
(24, '26', '444', '3123', '333', '2021-09-27 23:15:48', '2021-09-27 23:15:48', 1, NULL, NULL, NULL, 0),
(25, '27', '444', '123', '2312', '2021-09-27 23:17:09', '2021-09-27 23:17:09', 1, NULL, NULL, NULL, 0),
(26, '28', '13222', '314', 'aaaaaaaaa', '2021-09-27 23:18:12', '2021-09-27 23:18:12', 1, NULL, NULL, NULL, 0),
(27, '29', '124', '3123', '14123', '2021-09-27 23:20:54', '2021-09-27 23:20:54', 1, NULL, NULL, NULL, 0),
(28, '30', 'aaa', 'aaa', 'aaa', '2021-09-27 23:22:23', '2021-09-27 23:22:23', 1, NULL, NULL, NULL, 0),
(29, '31', '123', '1231', '3123', '2021-09-27 23:23:18', '2021-09-27 23:23:18', 1, NULL, NULL, NULL, 0),
(30, '32', '44', '3333', '22', '2021-09-27 23:24:11', '2021-09-27 23:24:11', 1, NULL, NULL, NULL, 0),
(31, '33', '1231', '44', '123', '2021-09-27 23:25:35', '2021-09-27 23:25:35', 1, NULL, NULL, NULL, 0),
(32, '34', '22222', '41231', '44444444', '2021-09-27 23:26:46', '2021-09-27 23:26:46', 1, NULL, NULL, NULL, 0),
(33, '35', 'asda', 'asdasd', 'asd', '2021-09-27 23:31:51', '2021-09-27 23:31:51', 1, NULL, NULL, NULL, 0),
(34, '36', '123', '2314', '123123', '2021-09-27 23:33:12', '2021-09-27 23:33:12', 1, NULL, NULL, NULL, 0),
(35, '37', 'sdasd', 'a', 'asdas', '2021-09-27 23:35:06', '2021-09-27 23:35:06', 1, NULL, NULL, NULL, 0),
(36, '38', '44', '123', '123', '2021-09-27 23:42:25', '2021-09-27 23:42:25', 1, NULL, NULL, NULL, 0),
(37, '39', '3123', '31231', '123123', '2021-09-27 23:43:02', '2021-09-27 23:43:02', 1, NULL, NULL, NULL, 0),
(38, '40', '123', '123123', '444', '2021-09-27 23:45:55', '2021-09-27 23:45:55', 1, NULL, NULL, NULL, 0),
(39, '41', '4123', '2312', '14', '2021-09-27 23:46:53', '2021-09-27 23:46:53', 1, NULL, NULL, NULL, 0),
(40, '43', '44', '3214', '123', '2021-09-27 23:53:56', '2021-09-27 23:53:56', 1, NULL, NULL, NULL, 0),
(41, '44', '12345678', 'AAAAAAAA updated', '0-=0-', '2021-09-27 23:55:24', '2021-09-27 23:55:24', 1, NULL, NULL, NULL, 0),
(42, '45', 'sdasd', 'asda', 'sdasd', '2021-09-27 23:56:30', '2021-09-27 23:56:30', 1, NULL, NULL, NULL, 0),
(43, '46', '3qwe', '312', '12312', '2021-09-27 23:59:30', '2021-09-27 23:59:30', 1, NULL, NULL, NULL, 0),
(44, '47', 'asdasd', 'asdas', 'asda', '2021-09-28 00:00:48', '2021-09-28 00:00:48', 1, NULL, NULL, NULL, 0),
(45, '48', 'asda33', 'asdasd333', 'asdasda', '2021-09-28 00:01:15', '2021-09-28 00:01:24', 1, NULL, NULL, NULL, 0),
(46, '49', '5454', '444', '545', '2021-09-28 00:03:09', '2021-09-28 00:03:09', 1, NULL, NULL, NULL, 0),
(47, '50', 'asdas', 'dasd', 'asda', '2021-09-28 00:06:07', '2021-09-28 00:06:07', 1, NULL, NULL, NULL, 0),
(48, '51', '123123', 'asdasd233', 'asda', '2021-09-28 00:10:10', '2021-09-28 00:10:10', 1, NULL, NULL, NULL, 0),
(49, '52', '6666666', '5555555', '87777777', '2021-09-28 00:10:58', '2021-09-28 00:10:58', 1, NULL, NULL, NULL, 0),
(50, '53', '2312311231', '23123', '323123', '2021-09-28 00:12:20', '2021-09-30 18:06:35', 2, '2021-10-01 02:06:35', NULL, NULL, 0),
(51, '54', 'sdf', 'dfsdf', 'sdf', '2021-09-28 00:34:35', '2021-09-28 00:34:35', 1, NULL, NULL, NULL, 0),
(52, '55', 'dddddddddd', 'ssssssss', 'ssss', '2021-09-28 00:51:26', '2021-09-28 00:51:26', 1, NULL, NULL, NULL, 0),
(53, '56', 'asdas', 'asdasd', 'dasdasdasd', '2021-09-28 01:02:42', '2021-09-28 01:02:42', 1, NULL, NULL, NULL, 0),
(54, '57', '45613', '4567897', '132154', '2021-09-28 19:47:18', '2021-09-30 17:49:22', 1, NULL, 'Problem with item', '2021-10-01 01:49:22', 1),
(55, '58', 'dasda', 'sdasdas', 'asdasd', '2021-09-28 19:49:48', '2021-09-28 19:49:48', 1, NULL, NULL, NULL, 0),
(56, '59', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-30 18:17:37', '2021-09-30 18:20:33', 2, '2021-10-01 02:20:33', NULL, NULL, 0),
(57, '60', '09351035872', 'AAAAAAAA updated', 'asssssssss', '2021-09-30 18:44:18', '2021-09-30 18:51:41', 2, '2021-10-01 02:51:41', NULL, NULL, 0),
(58, '61', '09351035872', 'Gabriel Anthony Gersalia updated', '2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', '2021-09-30 19:34:42', '2021-09-30 19:34:42', 1, NULL, NULL, NULL, 0),
(59, '62', '2222', 'BBB', 'AASSDWWEQ', '2021-09-30 20:15:03', '2021-09-30 20:15:03', 1, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2021_09_14_032338_create_permission_tables', 2),
(4, '0000_00_00_000000_create_websockets_statistics_entries_table', 3),
(5, '2021_09_17_033645_update_users_add_role_id', 4),
(6, '2021_09_17_040846_update_users_add_avatar', 5),
(7, '2021_09_17_080247_update_users_add_contact_no', 6),
(8, '2019_08_19_000000_create_failed_jobs_table', 7),
(9, '2021_09_22_090111_add_parcel_timeline_table', 8),
(10, '2021_09_28_015929_change_order_id_from_pickup_details', 9),
(11, '2021_09_28_020030_change_order_id_from_delivery_details', 9),
(13, '2021_10_01_035952_update_order_add_id_from_bentahan', 10);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tracking_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rider_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT 3,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `pickup_at` timestamp NULL DEFAULT NULL,
  `delivered_at` timestamp NULL DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `bentahan_order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `tracking_no`, `size`, `weight`, `total`, `rider_id`, `status_id`, `created_at`, `updated_at`, `completed_at`, `start_date`, `pickup_at`, `delivered_at`, `cancelled_at`, `bentahan_order_id`) VALUES
(53, '123123', '44123', '123', '123', '123213', 42, 11, '2021-09-28 00:12:20', '2021-09-30 18:08:38', '2021-10-01 02:08:38', '2021-09-29 23:55:36', NULL, NULL, NULL, NULL),
(57, 'aasdasdas', '0355778855222', '2', '231', '123', 42, 12, '2021-09-28 19:47:18', '2021-09-30 17:56:17', NULL, '2021-09-30 01:46:06', NULL, NULL, '2021-10-01 01:56:17', NULL),
(58, '23123', '3341516123151231', '123', '123', '123', 42, 9, '2021-09-28 19:49:48', '2021-09-30 00:23:34', NULL, '2021-09-30 00:23:34', NULL, NULL, NULL, NULL),
(59, 'O149-210814011340-WBS6X', '000000000001', 'xx', '1', '550', 42, 9, '2021-09-30 18:17:37', '2021-09-30 18:20:16', NULL, '2021-09-30 18:20:16', NULL, NULL, NULL, NULL),
(60, 'STU-14561-UZA', '9999999', '2', '3', '2500', 42, 11, '2021-09-30 18:44:18', '2021-09-30 18:52:55', '2021-10-01 02:52:55', '2021-09-30 18:49:15', NULL, NULL, NULL, NULL),
(61, 'O149-210814011340-WBS6X', '000000000001', '2x2x2', '1', '550', 42, 5, '2021-09-30 19:34:41', '2021-10-01 03:56:05', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'For Delivery', '2021-08-18 00:09:01', '2021-08-18 00:09:01'),
(2, 'Ready To Ship', '2021-08-18 00:09:15', '2021-08-18 00:09:15'),
(3, 'Pending', '2021-08-18 00:09:31', '2021-08-18 00:09:31'),
(4, 'Failed Delivery', '2021-08-18 00:09:45', '2021-08-18 00:09:45'),
(5, 'Delivered', '2021-08-18 00:09:54', '2021-08-18 00:09:54'),
(6, 'Processing Return', '2021-08-18 00:10:10', '2021-08-18 00:10:10'),
(7, 'Returning Item To Seller', '2021-08-18 00:10:19', '2021-08-18 00:10:19'),
(8, 'Returned', '2021-08-18 00:10:26', '2021-08-18 00:10:26'),
(9, 'Accepted', '2021-08-22 17:27:06', '2021-08-22 17:27:06'),
(10, 'Picked up from Sellers Ware House', '2021-08-22 18:09:02', '2021-08-22 18:09:02'),
(11, 'Completed', '2021-08-31 18:27:45', '2021-08-31 18:27:45'),
(12, 'Cancelled', '2021-09-06 14:50:32', '2021-09-06 14:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `parcel_timelines`
--

CREATE TABLE `parcel_timelines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parcel_timelines`
--

INSERT INTO `parcel_timelines` (`id`, `status`, `description`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Created', 'Order has been created', 58, '2021-09-28 19:49:49', '2021-09-28 19:49:49'),
(2, 'Accepted', 'The order has been accepted by the delivery rider', 53, '2021-09-29 23:55:36', '2021-09-29 23:55:36'),
(3, 'Accepted', 'The order has been accepted by the delivery rider', 58, '2021-09-30 00:23:34', '2021-09-30 00:23:34'),
(4, 'Accepted', 'The order has been accepted by the delivery rider', 57, '2021-09-30 01:46:06', '2021-09-30 01:46:06'),
(5, 'Cancelled Pickuped', 'Problem with item', 57, '2021-09-30 17:17:46', '2021-09-30 17:17:46'),
(6, 'Cancelled Pickuped', 'Can\'t contact Seller', 57, '2021-09-30 17:19:50', '2021-09-30 17:19:50'),
(7, 'Cancelled Pickuped', 'Can\'t contact Seller', 57, '2021-09-30 17:26:06', '2021-09-30 17:26:06'),
(8, 'Cancelled Pickuped', 'Can\'t contact Seller', 57, '2021-09-30 17:27:53', '2021-09-30 17:27:53'),
(9, 'Cancelled Pickuped', 'Can\'t contact Seller', 57, '2021-09-30 17:30:39', '2021-09-30 17:30:39'),
(10, 'Cancelled Pickuped', 'Can\'t contact Seller', 57, '2021-09-30 17:40:23', '2021-09-30 17:40:23'),
(11, 'Cancelled Pickuped', 'Problem with item', 57, '2021-09-30 17:42:39', '2021-09-30 17:42:39'),
(12, 'Cancelled Delivery', 'Problem with item', 57, '2021-09-30 17:46:14', '2021-09-30 17:46:14'),
(13, 'Cancelled Delivery', 'Problem with item', 57, '2021-09-30 17:49:22', '2021-09-30 17:49:22'),
(14, 'Picked Up', 'The item has been successfully picked up on 212312', 53, '2021-09-30 18:06:21', '2021-09-30 18:06:21'),
(15, 'Picked Up', 'The item has been successfully picked up on 212312', 53, '2021-09-30 18:06:21', '2021-09-30 18:06:21'),
(16, 'Picked Up', 'The item has been successfully picked up on 212312', 53, '2021-09-30 18:06:22', '2021-09-30 18:06:22'),
(17, 'Picked Up', 'The item has been successfully picked up on 212312', 53, '2021-09-30 18:06:22', '2021-09-30 18:06:22'),
(18, 'Delivered', 'The item has been successfully delivered on 323123', 53, '2021-09-30 18:06:34', '2021-09-30 18:06:34'),
(19, 'Delivered', 'The item has been successfully delivered on 323123', 53, '2021-09-30 18:06:34', '2021-09-30 18:06:34'),
(20, 'Delivered', 'The item has been successfully delivered on 323123', 53, '2021-09-30 18:06:35', '2021-09-30 18:06:35'),
(21, 'Delivered', 'The item has been successfully delivered on 323123', 53, '2021-09-30 18:06:35', '2021-09-30 18:06:35'),
(22, 'Delivered', 'The item has been successfully delivered on 323123', 53, '2021-09-30 18:06:35', '2021-09-30 18:06:35'),
(23, 'Completed', 'The order has been successfully completed', 53, '2021-09-30 18:08:38', '2021-09-30 18:08:38'),
(24, 'Accepted', 'The order has been accepted by the delivery rider', 59, '2021-09-30 18:20:16', '2021-09-30 18:20:16'),
(25, 'Delivered', 'The item has been successfully delivered on 2222 Bonifacio Street Bagong Silang CITY OF MANDALUYONG NCR (National Capital Region)', 59, '2021-09-30 18:20:33', '2021-09-30 18:20:33'),
(26, 'Created', 'Order has been created', 60, '2021-09-30 18:44:18', '2021-09-30 18:44:18'),
(27, 'Accepted', 'The order has been accepted by the delivery rider', 60, '2021-09-30 18:49:15', '2021-09-30 18:49:15'),
(28, 'Picked Up', 'The item has been successfully picked up on aaaaaaa', 60, '2021-09-30 18:49:29', '2021-09-30 18:49:29'),
(29, 'Delivered', 'The item has been successfully delivered on asssssssss', 60, '2021-09-30 18:51:41', '2021-09-30 18:51:41'),
(30, 'Completed', 'The order has been successfully completed', 60, '2021-09-30 18:52:41', '2021-09-30 18:52:41'),
(31, 'Completed', 'The order has been successfully completed', 60, '2021-09-30 18:52:44', '2021-09-30 18:52:44'),
(32, 'Completed', 'The order has been successfully completed', 60, '2021-09-30 18:52:55', '2021-09-30 18:52:55'),
(33, 'Created', 'Order has been created', 61, '2021-09-30 19:34:42', '2021-09-30 19:34:42'),
(34, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:37:52', '2021-09-30 19:37:52'),
(35, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:38:39', '2021-09-30 19:38:39'),
(36, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:42:20', '2021-09-30 19:42:20'),
(37, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:46:30', '2021-09-30 19:46:30'),
(38, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:48:54', '2021-09-30 19:48:54'),
(39, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:49:38', '2021-09-30 19:49:38'),
(40, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:50:26', '2021-09-30 19:50:26'),
(41, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:52:26', '2021-09-30 19:52:26'),
(42, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:52:46', '2021-09-30 19:52:46'),
(43, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:55:17', '2021-09-30 19:55:17'),
(44, 'Accepted', 'The order has been accepted by the delivery rider', 61, '2021-09-30 19:56:04', '2021-09-30 19:56:04'),
(45, 'Created', 'Order has been created', 62, '2021-09-30 20:15:03', '2021-09-30 20:15:03'),
(46, 'Created', 'Order has been created', 66, '2021-09-30 21:40:18', '2021-09-30 21:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pickup_details`
--

CREATE TABLE `pickup_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `picked_up_at` datetime DEFAULT NULL,
  `cancelled_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `isCancelled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pickup_details`
--

INSERT INTO `pickup_details` (`id`, `order_id`, `contact_no`, `contact_person`, `address`, `created_at`, `updated_at`, `status`, `picked_up_at`, `cancelled_reason`, `cancelled_at`, `isCancelled`) VALUES
(1, '1', '09351035872', 'Mark Anthony Gersalia', 'Siniloan Laguna', '2021-09-11 17:02:03', '2021-09-11 17:34:15', 1, NULL, NULL, '2021-09-12 01:34:15', 1),
(3, '2', '09351035872', 'Gabiel Anthony Gersalia', '23123', '2021-09-11 17:45:47', '2021-09-11 17:49:40', 1, NULL, 'Can\'t contact seller', '2021-09-12 01:49:40', 1),
(4, '3', '09351035872', 'Mark Anthony Gersalia', '23123', '2021-09-11 18:16:54', '2021-09-11 18:19:12', 2, '2021-09-12 02:19:12', NULL, NULL, 0),
(5, '4', '09351035872', 'Mark Anthony Gersalia', '23123', '2021-09-11 18:21:31', '2021-09-11 18:21:50', 2, '2021-09-12 02:21:50', NULL, NULL, 0),
(6, '5', '54587548', 'asd,amnsdaweui', '24adjasdKJSHDJKHASd', '2021-09-18 18:18:28', '2021-09-18 18:18:28', 1, NULL, NULL, NULL, 0),
(7, '6', '000000000231', 'AAAAAAAA updated', 'AAAAAAAAAAAAAA A', '2021-09-18 20:29:03', '2021-09-19 20:16:03', 1, NULL, NULL, NULL, 0),
(8, '7', '55488978', 'asdawdasd', 'asda4548a7w8e54', '2021-09-18 20:45:09', '2021-09-18 20:45:09', 1, NULL, NULL, NULL, 0),
(9, '8', 'aaaaaaa', 'aaaaa updated', 'aaaaaaaaa', '2021-09-19 02:13:15', '2021-09-27 00:48:45', 1, NULL, NULL, NULL, 0),
(10, '11', 'seller no', 'Bentahan seller', 'seller address', '2021-09-27 18:08:04', '2021-09-27 18:08:04', 1, NULL, NULL, NULL, 0),
(11, '12', 'seller no', 'Bentahan seller', 'seller address', '2021-09-27 19:02:11', '2021-09-27 19:02:11', 1, NULL, NULL, NULL, 0),
(12, '13', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:04:06', '2021-09-27 19:04:06', 1, NULL, NULL, NULL, 0),
(13, '14', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:06:44', '2021-09-27 19:06:44', 1, NULL, NULL, NULL, 0),
(14, '15', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:08:16', '2021-09-27 19:08:16', 1, NULL, NULL, NULL, 0),
(15, '16', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:08:24', '2021-09-27 19:08:24', 1, NULL, NULL, NULL, 0),
(16, '17', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:09:47', '2021-09-27 19:09:47', 1, NULL, NULL, NULL, 0),
(17, '18', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:10:46', '2021-09-27 19:10:46', 1, NULL, NULL, NULL, 0),
(18, '19', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:32:57', '2021-09-27 19:32:57', 1, NULL, NULL, NULL, 0),
(19, '20', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-27 19:33:15', '2021-09-27 19:33:15', 1, NULL, NULL, NULL, 0),
(20, '21', '23123', '231231', '123123', '2021-09-27 19:40:03', '2021-09-27 19:40:03', 1, NULL, NULL, NULL, 0),
(21, '22', '1231', '231231', 'dasd', '2021-09-27 22:44:25', '2021-09-27 22:44:25', 1, NULL, NULL, NULL, 0),
(22, '23', '2123', 'aasdasd', '123', '2021-09-27 22:45:02', '2021-09-27 22:45:02', 1, NULL, NULL, NULL, 0),
(23, '24', '123123', '12312', '1231231', '2021-09-27 22:47:50', '2021-09-27 22:47:50', 1, NULL, NULL, NULL, 0),
(24, '25', 'asdasd', 'asd', 'asdasd23123', '2021-09-27 23:15:19', '2021-09-27 23:15:19', 1, NULL, NULL, NULL, 0),
(25, '26', '444', '23123', '23123', '2021-09-27 23:15:48', '2021-09-27 23:15:48', 1, NULL, NULL, NULL, 0),
(26, '27', '23123123', 'sad213', '31231', '2021-09-27 23:17:09', '2021-09-27 23:17:09', 1, NULL, NULL, NULL, 0),
(27, '28', '123124123', '231', 'aaaa', '2021-09-27 23:18:12', '2021-09-27 23:18:12', 1, NULL, NULL, NULL, 0),
(28, '29', '123', '2312', '123', '2021-09-27 23:20:54', '2021-09-27 23:20:54', 1, NULL, NULL, NULL, 0),
(29, '30', 'aa', 'aa', 'aaa', '2021-09-27 23:22:24', '2021-09-27 23:22:24', 1, NULL, NULL, NULL, 0),
(30, '31', '123123', '3123', '33', '2021-09-27 23:23:18', '2021-09-27 23:23:18', 1, NULL, NULL, NULL, 0),
(31, '32', '333', '4123', '22', '2021-09-27 23:24:11', '2021-09-27 23:24:11', 1, NULL, NULL, NULL, 0),
(32, '33', '123123', '1231', '1234', '2021-09-27 23:25:35', '2021-09-27 23:25:35', 1, NULL, NULL, NULL, 0),
(33, '34', '11111111', '32123', '22333333333', '2021-09-27 23:26:46', '2021-09-27 23:26:46', 1, NULL, NULL, NULL, 0),
(34, '35', 'asdasd', 'asdasd', 'asda', '2021-09-27 23:31:51', '2021-09-27 23:31:51', 1, NULL, NULL, NULL, 0),
(35, '36', '123123', '231', '123', '2021-09-27 23:33:12', '2021-09-27 23:33:12', 1, NULL, NULL, NULL, 0),
(36, '37', 'aaa', 'aq', 'asd', '2021-09-27 23:35:06', '2021-09-27 23:35:06', 1, NULL, NULL, NULL, 0),
(37, '38', '13', '231', '2312', '2021-09-27 23:42:25', '2021-09-27 23:42:25', 1, NULL, NULL, NULL, 0),
(38, '39', '231231', '2312312', '123123', '2021-09-27 23:43:02', '2021-09-27 23:43:02', 1, NULL, NULL, NULL, 0),
(39, '40', '123123', '131', '1231', '2021-09-27 23:45:55', '2021-09-27 23:45:55', 1, NULL, NULL, NULL, 0),
(40, '41', '2312314', 'awe1', '123123123', '2021-09-27 23:46:53', '2021-09-27 23:46:53', 1, NULL, NULL, NULL, 0),
(41, '43', '11', '231', '123', '2021-09-27 23:53:56', '2021-09-27 23:53:56', 1, NULL, NULL, NULL, 0),
(42, '44', '12345678', 'AAAAAAAA updated', '0-0', '2021-09-27 23:55:24', '2021-09-27 23:55:24', 1, NULL, NULL, NULL, 0),
(43, '45', 'sda', 'asdas', 'asda', '2021-09-27 23:56:30', '2021-09-27 23:56:30', 1, NULL, NULL, NULL, 0),
(44, '46', 'w123', '2312', '12asda', '2021-09-27 23:59:30', '2021-09-27 23:59:30', 1, NULL, NULL, NULL, 0),
(45, '47', 'dasdasd', 'asd', 'asd', '2021-09-28 00:00:48', '2021-09-28 00:00:48', 1, NULL, NULL, NULL, 0),
(46, '48', 'asdasd', 'asdasd233', 'asdasd', '2021-09-28 00:01:15', '2021-09-28 00:01:24', 1, NULL, NULL, NULL, 0),
(47, '49', '45', '4545', '45454', '2021-09-28 00:03:09', '2021-09-28 00:03:09', 1, NULL, NULL, NULL, 0),
(48, '50', 'dasda', 'sd', 'sdasd', '2021-09-28 00:06:07', '2021-09-28 00:06:07', 1, NULL, NULL, NULL, 0),
(49, '51', '123123', 'asdasd', 'sdasdasd', '2021-09-28 00:10:10', '2021-09-28 00:10:10', 1, NULL, NULL, NULL, 0),
(50, '52', '56666666', '444444444', '666888888', '2021-09-28 00:10:58', '2021-09-28 00:10:58', 1, NULL, NULL, NULL, 0),
(51, '53', '23123', '1231', '212312', '2021-09-28 00:12:20', '2021-09-30 18:06:22', 2, '2021-10-01 02:06:22', NULL, NULL, 0),
(52, '54', 'sdf', 'dfsd', 'sdf', '2021-09-28 00:34:35', '2021-09-28 00:34:35', 1, NULL, NULL, NULL, 0),
(53, '55', 'sddddddddd', 'ssssssssssAAADDDSSSWEQWE', 'sssaaaaaa', '2021-09-28 00:51:26', '2021-09-28 00:53:47', 1, NULL, NULL, NULL, 0),
(54, '56', 'dasdasdasd', 'asdasd', 'dasdasDDDDDDDDSSSSSSSSSSSSS', '2021-09-28 01:02:43', '2021-09-28 01:02:43', 1, NULL, NULL, NULL, 0),
(55, '57', '123456', '123456', '123456', '2021-09-28 19:47:18', '2021-09-30 17:42:39', 1, NULL, 'Problem with item', '2021-10-01 01:42:39', 1),
(56, '58', 'dasdas', 'asasda', 'sdasdas', '2021-09-28 19:49:48', '2021-09-28 19:49:48', 1, NULL, NULL, NULL, 0),
(57, '60', '09351035872', 'aaa', 'aaaaaaa', '2021-09-30 18:44:18', '2021-09-30 18:49:29', 2, '2021-10-01 02:49:29', NULL, NULL, 0),
(58, '61', '0912315123', 'bentahan.ph', 'pasig city', '2021-09-30 19:34:42', '2021-09-30 19:34:42', 1, NULL, NULL, NULL, 0),
(59, '62', '2222', 'AAAA', 'AADDD', '2021-09-30 20:15:03', '2021-09-30 20:15:03', 1, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', NULL, NULL),
(2, 'Staff', 'staff', NULL, NULL),
(3, 'Delivery Rider', 'rider', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT 2,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `avatar`, `contact_no`) VALUES
(1, 'Mark', 'admin@admin.com', '$2y$10$zIVbM34hiFQYlg9N4Sjbx.bWOezIRwQb13qOR1l4peGDrNOSsly3.', NULL, '2021-09-13 17:53:28', '2021-09-27 01:23:32', 1, '/uploads/avatar/1/mark.jpg', NULL),
(3, 'staff 1', 'staff@logistics.com', '$2y$10$TYKNAuwb0PwA22QXkG6tKOiqo0klcHDKu6b8hF1fio3Kr.QDG5ody', NULL, '2021-09-15 19:31:34', '2021-09-15 19:31:34', 2, NULL, NULL),
(4, 'staff 2', 'staff2@logistics.com', '$2y$10$gS0R29/fqxxJLOCqdEHjZu6mkMOrfYIPluFJZqxuqNefjecxmnd3.', NULL, '2021-09-15 19:32:35', '2021-09-15 19:32:35', 2, NULL, NULL),
(17, 'Rider 3', 'rider3@logistics.com', '$2y$10$IFt7peS97pQJsLcwnRr96.PtewA.TNwlgxGQgkDus7enip5Kmq6n.', NULL, '2021-09-17 23:43:25', '2021-09-17 23:43:25', 3, '/uploads/avatar//rider-3.jpg', '12345678'),
(37, 'asdasdasd', 'asdasd@asdasd', '$2y$10$3IJt566S46fRRXzGhPGcP./m0U0ISrpiq1Qjk/OiToLwG6Qq0q/Zu', NULL, '2021-09-28 18:54:42', '2021-09-28 18:54:42', 3, '/uploads/avatar//asdasdasd.jpg', 'asdasd'),
(41, 'aaaaa', 'aaa@311231', '$2y$10$ZWNETZEjbSPb6QMmXdZSmuE93Tpur4TQ5PaDVDG4KoTNkWAdjafLi', NULL, '2021-09-28 19:05:00', '2021-09-28 19:05:00', 3, '/uploads/avatar//aaaaa.png', '123123'),
(42, 'rider 2', 'rider2@logistics.com', '$2y$10$VZ4Q114AahfCKRrq9GYHwuhDtxatOogpPdWj4zAc5HICB75dP/gzm', NULL, '2021-09-28 22:49:06', '2021-09-28 22:49:06', 3, NULL, '09351035872'),
(43, 'Rider 1', 'rider1@logistics.com', '$2y$10$bwiHkMV5gT45jVP2rqr6iuUxShHQOCmVcDwuZg6s9eNPIdPIZ6UMW', NULL, '2021-09-29 00:25:43', '2021-09-29 00:25:43', 3, NULL, '12345678'),
(44, 'qeqw', 'eqweq@asdasdasdasd', '$2y$10$ZcZ4w.Q9JgCxczja4AgMz.87TVEukiuwcMrNjdqnAT6wjwkT6ipe.', NULL, '2021-09-29 00:27:50', '2021-09-29 00:27:50', 3, NULL, 'eqwe');

-- --------------------------------------------------------

--
-- Table structure for table `websockets_statistics_entries`
--

CREATE TABLE `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_details`
--
ALTER TABLE `delivery_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parcel_timelines`
--
ALTER TABLE `parcel_timelines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pickup_details`
--
ALTER TABLE `pickup_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_details`
--
ALTER TABLE `delivery_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `parcel_timelines`
--
ALTER TABLE `parcel_timelines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `pickup_details`
--
ALTER TABLE `pickup_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
