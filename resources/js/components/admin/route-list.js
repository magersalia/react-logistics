
import Dashboard from './pages/Dashboard';
import Users from './pages/Users/index';
import UsersCreate from './pages/Users/Create';
import UsersEdit from './pages/Users/Edit';
 
import Riders from './pages/Riders/index';
import RidersCreate from './pages/Riders/Create';
import RidersEdit from './pages/Riders/Edit';


import Orders from './pages/Orders/index';
import OrdersCreate from './pages/Orders/Create';
import OrdersEdit from './pages/Orders/Edit';
 
const  admin_route_list = [
    {
        component: Dashboard,
        title:'Dashboard',
        path:'/admin/dashboard'
      },


      {
        component: Users,
        title:'Users list',
        exact:true,
        path:'/admin/users',
      },
      {
        component: UsersCreate,
        title:'Users',
        exact:true,
        path:'/admin/users/create',
      },
      {
        component: UsersEdit,
        title:'Users',
        exact:true,
        path:'/admin/users/edit/:id',
      }, 


      {
        component: Riders,
        title:'Riders list',
        exact:true,
        path:'/admin/riders',
      },
      {
        component: RidersCreate,
        title:'Riders',
        exact:true,
        path:'/admin/riders/create',
      },
      {
        component: RidersEdit,
        title:'Riders',
        exact:true,
        path:'/admin/riders/edit/:id',
      }, 

      {
        component: Orders,
        title:'Orders List',
        exact:true,
        path:'/admin/orders',
      },
      {
        component: OrdersCreate,
        title:'Orders',
        exact:true,
        path:'/admin/orders/create',
      },
      {
        component: OrdersEdit,
        title:'Orders',
        exact:true,
        path:'/admin/orders/edit/:id',
      },
];



const orders = [  ]

  


export default admin_route_list;