import React, { Component } from 'react'

export class Dashboard extends Component {
    render() {
        return (
            <div>
                <h1>Im the dashboard</h1>
                <button className="btn btn-primary shadow">
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  <span class="sr-only">Loading...</span></button>
            </div>
        )
    }
}

export default Dashboard
