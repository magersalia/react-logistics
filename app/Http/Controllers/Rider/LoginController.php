<?php

namespace App\Http\Controllers\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function index(){
        if (Auth::check()) {
            // Authentication passed...
            return redirect('rider/home');

        }else{
            return view('auth.login');

        } 
    }


     /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('contact_no', 'password'); 
        if (Auth::attempt($credentials)) { 
            if(Auth::user()->role_id === 3){
                // Authentication passed...
                return redirect()->intended('/rider/home');
    
            }else{
                Auth::logout();
                return redirect()->back()->withErrors(['message'=>'Login credentials not found']);
            }
        }else{
            return redirect()->back()->withErrors(['message'=>'Login credentials not found']);
        } 
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/admin');
    }
}
