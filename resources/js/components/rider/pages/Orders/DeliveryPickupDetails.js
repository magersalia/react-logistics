import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
  
import { PlusIcon,ClipboardListIcon } from '@heroicons/react/solid'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle,faList,faExclamationTriangle,faMapMarker,faPhone } from '@fortawesome/free-solid-svg-icons'
import { faWaze } from '@fortawesome/free-brands-svg-icons'
import ReasonModal from './ReasonModal'
 
function Index (props) {
    const {data,statusId,type} = props;
    const buttonText = type == 'pickup' ? 'Pick up' : 'Delivered'; 
    let [showModal,setShowModal] = useState(false);

    const saveSuccess = () =>{
        props.reloadData && props.reloadData()
        toggleModal()
    }


    const toggleModal = () =>{
        setShowModal(!showModal); 
    }

    const updateStatus = (status) =>{ 
        props.toggleLoading(true) && props.toggleLoading
        let id = data.id,
        url = `/api/rider/orders/${type}/update/${id}/${status}`;
        axios.get(url)
        .then(response => {  
            
        props.toggleLoading(false) && props.toggleLoading
            props.reloadData && props.reloadData()
        })
        .catch(error => { 
            if (error.response.status == 422) {
                // setFormErrors(error.response.data.errors)
            } 
        });
    }

    return(
        <div>
            {
            showModal ?
            <ReasonModal 
            open={showModal}
            type={type}
            id={data.id}
            toggleModal={toggleModal}  
            saveSuccess={saveSuccess}
            toast={props.toast}
            />
            :
            null
          } 

        <ul className="divide-y transition-all	 divide-gray-300 border rounded mb-5">
            <li className="p-3 hover:bg-gray-50 cursor-pointer">
                <span className="font-bold  block">Address:</span> 
                <span className="block mb-1">{data.address} </span>
                <a target="_blank" className="bg-red-500 px-1 rounded text-white p-1 shadow-sm mr-1" href={`https://maps.google.com/?q=${data.address }`}> <small><FontAwesomeIcon icon={faMapMarker} className="mr-1"/>Open in Maps </small></a>
                <a href={`https://www.waze.com/ul?q=${data.address}`} target="_blank" className="text-white bg-blue-300 px-1 py-1 rounded p-1 shadow-sm"><small><FontAwesomeIcon icon={faWaze} className="mr-1"/>Open in waze</small></a>
            </li>
            <li className="p-3 hover:bg-gray-50 cursor-pointer"> 
                <span className="font-bold  block">Name:</span> 
                <span className="block mb-1">{data.contact_person} </span>
            </li>
            <li className="p-3 hover:bg-gray-50 cursor-pointer">
                <span className="font-bold  block">Contact:</span> 
                <span className="block mb-1">{data.contact_no} </span>
            <a className="text-green-500" href={`tel:${data.contact_no}`}><u><FontAwesomeIcon icon={faPhone} />Make Call</u></a>
            </li> 
        </ul>  
        {
            !data.isCancelled ?
                <div>
                    {
                        statusId == 9 ? 
                            data.status == 1 ?
                            <div>
                                <button onClick={()=>updateStatus(2)} className="bg-green-500 text-white rounded-full shadow-lg px-2 py-2 block w-full mb-3">{buttonText}</button>
                                <button onClick={()=>setShowModal(true)} className="bg-red-500 text-white rounded-full shadow-lg px-2 py-2 block w-full">Cancel</button>
                            </div>
                                :<div className="bg-green-300  px-2 py-3 rounded text-center text-white">
                                    The parcel has been {type == 'delivery' ? 'delivered' : type}
                                </div>
                        : null
                    }
                    </div>
                :
                <div className="bg-red-500 w-full px-2 py-3 rounded text-center text-white">
                <h5 className=" text-3xl mb-3">
                    <FontAwesomeIcon icon={faExclamationTriangle} className="block mx-auto" />
                    The {type} is cancelled
                </h5>
                    <strong className="block">Reason:</strong>
                    <span className="block">
                    {data.cancelled_reason} 
                    </span>
                    <strong className="block">Cancelled at:</strong>
                    <span className="block">
                    {data.cancelled_at} 
                    </span> 
            </div> 
        }
        </div>
    )

    }
    
Index.propTypes = {}

export default Index;
 