import React,{useState,useEffect} from 'react';

 
import DataTable from './DataTable';
import { PlusIcon } from '@heroicons/react/solid'


function Index (props) {

  let [users,setUsers] = useState(null);
  let [loading,setLoading] = useState(false);
 
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
 

  
  useEffect(() => { 
    getData() 
}, []);

 
  
  return ( 
      <div>
        {
          loading ?
            <div class="w-full h-full fixed block top-0 left-0 bg-gray-800 opacity-75">
              <span class="text-green-500 z-99 top-1/2 my-0 mx-auto block relative w-0 h-0"
              style={{top: '50%'}}>
              <div class="loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-10 w-10"></div>
              </span>
            </div>
            :
            ''
        }
        
        <header className="bg-white shadow overflow-hidden border-b border-gray-200 rounded mb-4 shadow-sm">
        <div className="mx-auto py-3 px-3 sm:px-6 lg:px-8 grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-gray-900 mt-2">
          Users</h1>
        </div>
        <div className="text-right"> 
        <button onClick={()=>alert('HEY')} className={`bg-blue-600 hover:bg-blue-700 text-white  py-2 px-2 rounded pull-right`}>
                     Add User
                    </button> 
        </div>
        </div>
      </header>

        <DataTable data={users} heading={tableHeading} />
      
      </div>
  );
}

Index.propTypes = {}

export default Index;

