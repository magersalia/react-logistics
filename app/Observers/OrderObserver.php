<?php

namespace App\Observers;

use App\Order;
use App\PickupDetail;
use App\DeliveryDetail;

class OrderObserver
{
    /**
     * Handle the orders "created" event.
     *
     * @param  \App\orders  $orders
     * @return void
     */
    public function created(Order $order)
    {
        //
        // $id = $order->id;

        // if(request()->has("delivery_details")){
        //     $data = request()->get('delivery_details'); 
        //     $data['order_id'] = $id; 
        //     DeliveryDetail::create($data);
        // }

        // if(request()->has("pickup_details")){
        //     $data = request()->get('pickup_details');
        //     $data['order_id'] = $id;
        //     PickupDetail::create($data);
        // }

    }

    /**
     * Handle the orders "updated" event.
     *
     * @param  \App\orders  $orders
     * @return void
     */
    public function updated(Order $orders)
    {
        //
    }

    /**
     * Handle the orders "deleted" event.
     *
     * @param  \App\orders  $orders
     * @return void
     */
    public function deleted(Order $orders)
    {
        //
    }

    /**
     * Handle the orders "restored" event.
     *
     * @param  \App\orders  $orders
     * @return void
     */
    public function restored(Order $orders)
    {
        //
    }

    /**
     * Handle the orders "force deleted" event.
     *
     * @param  \App\orders  $orders
     * @return void
     */
    public function forceDeleted(Order $orders)
    {
        //
    }
}
