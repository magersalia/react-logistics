<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //
    public function index(){
        // event(new \App\Events\RealTimeMessage('Hello World'));
        $users = User::with('role')->whereIn('role_id',[2,1])->latest()->get();
        return response()->json(['message' => 'OK', 'data' => $users]);   
    }

    public function create(Request $request){
        // $allData = $request->all();
 
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->password = bcrypt($request->password); 
            
        // $allData = $allData->except(['_token','role']);
        if($request->avatar || $request->hasFile('avatar')){
            $avatar = $this->saveAvatar($user, $request->avatar);
            $user->avatar = $avatar['path'].$avatar['filename'];
        }

        $user->save();
        // $allData['password'] = bcrypt($request->password);    
        // User::create($allData);
        // event(new \App\Events\RealTimeMessage('Hello World'));

        return response()->json(['success' => true]);   

    }

    public function edit(Request $request){
        $user = User::find($request->id);
        return response()->json(['message' => 'OK', 'data' => $user]);  
    }


    public function update(Request $request){ 
        
        $user = User::find($request->id); 
          $allData = collect(request()->all())->except(['avatar']);
        if($request->hasFile('avatar')){
            $avatar = $this->saveAvatar($user, $request->avatar);
            $allData['avatar'] = $avatar['path'].$avatar['filename'];
        }
  
        $user->update($allData->toArray());

        return response()->json(['success' => true]);  
    }
    
    public function saveAvatar($data, $file)
    {
        try { 
            $base_dir = public_path();
            $id = $data->id;
            $full_dir = $base_dir . '/uploads/avatar/'. $id .'/';
             
            if (!is_dir($full_dir)) {
                mkdir($full_dir, 0777, true);
            }

              

            $extension = strtolower($file->getClientOriginalExtension());
            $filename = \Str::slug($data->name).'.'.$extension; 

            $image_filename = $filename;

            $file->move($full_dir, $image_filename);

            $path = '/uploads/avatar/'. $id .'/';
             
            return [
                'filename' => $image_filename,
                'path' => $path
            ];

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            throw $e;
        }
    }
    
   public function delete(Request $request){
        $order = User::whereId($request->id)->delete();
        return response()->json(['message' => 'OK']);  
    } 
}
