import React, { useState,Fragment } from 'react';  

import { Disclosure, Menu, Transition } from '@headlessui/react'
import { BellIcon, MenuIcon, XIcon,HomeIcon } from '@heroicons/react/outline'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBoxOpen,faChevronLeft } from '@fortawesome/free-solid-svg-icons'


import { NavLink, Link, useHistory } from 'react-router-dom';
  

import Avatar from 'react-avatar';
import Breadcrumbs from './Breadcrumbs';


 function  Nav (props){
   const {title} = props;
    let history = useHistory(); 
      const userNavigation = [
        { name: 'Your Profile', href: '#' },
        { name: 'Settings', href: '#' },
        { name: 'Sign out', href: '#' },
      ]
      function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
      }
      const handleLogout = () => { 
        window.location.href = '/admin/logout'; 
      };
        return (
            <div>
            <Disclosure as="nav" className="bg-white  border-b border-gray-200  ">
            {/* <Disclosure as="nav" className="bg-gray-800"> */}
                   {({ open }) => (                                                      
                     <>
                       <div className="lg:w-1/2  mx-auto  px-2 ">
                         <div className="flex items-center justify-between h-16">
                           <div className="flex items-center">
                             <div className="flex-shrink-0">
                               {
                                 title ?
                                  
                                <div>
                                 <Link to='/rider/home'>
                                <FontAwesomeIcon   className="mr-3" icon={faChevronLeft} />
                                </Link>
                                <strong className="font-bold text-green-500">{title} </strong> 
                                </div>
                                 :
                                <div>
                                 <FontAwesomeIcon   className="text-green-500 mr-2" icon={faBoxOpen} />
                                 <strong className="text-green-500 font-bold">BENTAHAN </strong>
                                 <span className="text-gray-500 text-shadow">Logistics</span>
                                 </div>
                               }
                             </div> 
                           </div>
                           <div className="">
                             <div className="ml-4 flex items-center md:ml-6"> 
           
                               {/* Profile dropdown */}
                               <Menu as="div" className="ml-3 relative">
                                 <div>
                                   <Menu.Button className="bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                     <span className="sr-only">Open user menu</span> 
                                     <Avatar 
                             
                                     src={user.avatar}   
                                     name={user.name}  
                                      size={30} round="100%" />
                                   </Menu.Button>
                                    
                                 </div>
                                 <Transition
                                   as={Fragment}
                                   enter="transition ease-out duration-100"
                                   enterFrom="transform opacity-0 scale-95"
                                   enterTo="transform opacity-100 scale-100"
                                   leave="transition ease-in duration-75"
                                   leaveFrom="transform opacity-100 scale-100"
                                   leaveTo="transform opacity-0 scale-95"
                                 >
                                   <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow  py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                                     {userNavigation.map((item) => (
                                       <Menu.Item key={item.name}>
                                         {({ active }) => (
                                           <a
                                             href={item.href}
                                             className={classNames(
                                               active ? 'bg-gray-100' : '',
                                               'block px-4 py-2 text-sm text-gray-700'
                                             )}
                                             onClick=
                                             {
                                               item.name == 'Sign out' ? handleLogout : ''
                                             }
                                           >
                                             {item.name}
                                           </a>
                                         )}
                                       </Menu.Item>
                                     ))}
                                   </Menu.Items>
                                 </Transition>
                               </Menu>
                             </div>
                           </div>
                            
                         </div>
                       </div>
            
                     </>
                   )}
                 </Disclosure> 
                    
                 </div>
                 
        ) 
}


export default Nav