import React from 'react';
import { Route, Redirect } from 'react-router-dom'; 
import PropTypes from 'prop-types'; 
import Nav from './pages/Nav'
function AuthRoute ({ component: Component, title, ...rest }) { 
  return ( 
    <Route
    {...rest}
    render={props => 
      {
        return (

          <div className="flex flex-col min-h-screen px-8">
          <Component {...props} />
        </div>  
        )
      } 
    }
  />
  );
};

AuthRoute.displayName = 'Auth Route';

AuthRoute.propTypes = {
  component: PropTypes.func.isRequired,
  rest: PropTypes.object,
  location: PropTypes.object,
  title: PropTypes.string
};

export default AuthRoute;
