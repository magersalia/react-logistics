@extends("auth") 
@section('content')
    <div class="container ">
        <div class="row">
            <div class="lg:w-3/12 mx-auto lg:mt-20">
                <h2 class="my-3 text-center  text-4xl">
                <i class="fa fa-box-open text-green-500"></i><br>
                   <strong class="text-green-500"> Bentahan</strong><br> Logistics<br>
                   
                <span class="text-sm text-center mx-auto">Rider Login</span>
                </h2>
                <div class="bg-white p-5 shadow-lg rounded mt-10">
                    <form action="{{url('/login/authenticate')}}" method="post">
                        @csrf
                         
                        <div class="mb-3">
                        <label for="exampleInputEmail1  @if ($errors->any()) text-red-500 @endif">Cellphone No:</label>
                        <input type="text" name="contact_no"  class="py-2 px-2 border mt-2 w-full @if($errors->any()) border-red-500 @else  border-gray-200 @endif rounded" id="exampleInputEmail1" aria-describedby="emailHelp">
                        @if ($errors->any())
                                <small id="emailHelp" class="form-text text-red-500">{{$errors->first()}}</small>
                        @endif
                        </div>
                        <div class="mb-3">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="py-2 px-2 border mt-2 border-gray-200 w-full rounded" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="px-3 py-2 rounded bg-indigo-500 rounded text-white">Submit</button>
                    </form>
                </div>

            </div>
        </div> 
    </div>
@endsection