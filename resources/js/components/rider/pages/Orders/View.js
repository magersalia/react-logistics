import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
  
import { PlusIcon,ClipboardListIcon } from '@heroicons/react/solid'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle,faList,faExclamationTriangle,faMapMarker,faPhone } from '@fortawesome/free-solid-svg-icons'
import { faWaze } from '@fortawesome/free-brands-svg-icons'

import DeliveryPickupDetails from './DeliveryPickupDetails'
import Spinner from '../../../elements/Spinner';
// import Loader from '../Loader';
import Nav from '../../pages/Nav' 
function Index (props) {


  let [order,setOrder] = useState(null);
  let [pickupDetails,setPickupDetails] = useState(null);
  let [deliveryDetails,setDeliveryDetails] = useState(null);

  let [selectedNav,setSelectedNav] = useState(1);  
  let [loading,setLoading] = useState(false);
 
  function classNameNames(...classNamees) {
    return classNamees.filter(Boolean).join(' ')
  }

  const toggleLoading = (status) =>{
      setLoading(status);
  }
 

  const getData = () =>{
    let id = props.match.params.id,
        url = `/api/rider/orders/details/${id}`;
        axios.get(url)
        .then(response => {   
          setOrder(response.data.data)     
          setDeliveryDetails(response.data.data.delivery_details)
          setPickupDetails(response.data.data.pickup_details) 
        })
        .catch(error => { 
            if (error.response.status == 422) {
                // setFormErrors(error.response.data.errors)
            } 
        });
  }


  useEffect(() => { 
    getData() 
}, []);


const updateStatus = (status) =>{
    toggleLoading(true)
    let id = props.match.params.id, 
    url = `/api/rider/orders/update-status/${id}/${status}`;
    axios.get(url)
    .then(response => {  
        getData()
        toggleLoading(false)
        
    })
    .catch(error => { 
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        } 
    });
}
 
   
const nav = [
    
    {
        name:'Pickup Details', 
        id:1
    },
    {
        name:'Delivery Details',
        id:2
    }, 
]

const handleSelected = (id)=>{
    setSelectedNav(id) 
    getData(id)
}
  return ( 
      <div className="mb-5"> 
      {
          loading ?
          <Spinner /> 
        : 
        null
      }
          
          <Nav title="Order Details" goBack={true}/> 

      
        {
            order ?

            <div className="mt-5 px-2"> 
            <div className="mb-5"> 
                <button hidden={order.status_id != 3 } disabled={loading} onClick={()=>updateStatus(9)} className={`px-3 py-2 rounded shadow  bg-yellow-500 hover:bg-yellow-700 text-white  w-full`}>Accept this order</button>
            </div>

            <div hidden={order.status_id == 3 }  className="bg-white px-2 py-5 text-center mb-3 shadow-md rounded">
            
            {
                order.status_id == 9 || order.status_id == 11 ? 
                <h2 className="text-green-500 font-medium ">
                <FontAwesomeIcon className="block mx-auto mb-2 text-3xl " icon={faCheckCircle} />
                <span >Order is {order.status.name}</span>            
            </h2> 
            :
            (order.status_id == 12  ?
                <h2 className="text-red-500 font-medium ">
                <FontAwesomeIcon className="block mx-auto mb-2 text-3xl " icon={faExclamationTriangle} />
                <span >Order is Cancelled</span>            
                </h2> 
                : 
                <h2 className="text-green-500 font-medium ">
                <span >Order is {order.status.name}</span>            
                </h2> 
            )
                
            }
            <div className="mt-2"> 
                {
                  order.pickup_details.status == 2 ?
                  <p>
                  <strong className="block">The parcel has been pickup on :</strong> 
                  <p>{order.pickup_details.picked_up_at}</p>
                  </p>
                  :null 
                }
                  {
                  order.delivery_details.status == 2 ?
                  <p>
                  <strong className="block">The parcel has been delivered on :</strong> 
                  <p>{order.delivery_details.delivered_at}</p>
                  </p>
                  :null 
                } 
                
    <div className="block mt-3" hidden={order.status_id == 3}>
        <strong>Duration:</strong>
    {order.duration}
     </div>

    {
        order.pickup_details.status == 2 && order.delivery_details.status == 2 && order.status_id != 11 ?
        <button onClick={()=>updateStatus(11)} className="px-2 py-2 bg-green-500 text-white w-full rounded shadow mt-2">Order is complete</button>

            // <a  href="{{route('order.update',[$order->id,11])}}"class="btn btn-success text-white btn-block btn-round shadow mt-2">Order is complete</a>
        :
            null
    }

    {
        order.pickup_details.isCancelled  && order.delivery_details.isCancelled && order.status_id != 12  ?
                <button onClick={()=>updateStatus(12)} className="px-2 py-2 bg-red-500 text-white w-full rounded shadow mt-2">Cancel This Order</button>
            // <a  href="{{route('order.update',[$order->id,11])}}"class="btn btn-success text-white btn-block btn-round shadow mt-2">Order is complete</a>
        :
            null
    }
            </div>
            </div>
            <div className="mb-3">
                <div className="bg-white border border-gray-300 rounded">
                    <div className="bg-white font-bold px-2 py-3 rounded-t border-b border-gray-300">
                        <FontAwesomeIcon icon={faList} className="mr-2"/>Order Details
                    </div>
                    <div className="bg-white px-2 py-4 border-b border-gray-300 rounded-b">
                        <div className="grid grid-cols-2 gap-4">
                            <div>
                                <small>
                                <span className="font-bold block">Order ID: </span>
                                    {order.order_id}
                                </small>
                            
                                <small>
                                <span className="font-bold block mt-3">Tracking No:</span>
                                {order.tracking_no}
                                </small>
                            </div>

                            <div>
                            <small>
                                <span className="font-bold block">Amount: </span>
                                    {order.total}
                                </small>
                            
                                <small>
                                <span className="font-bold block mt-3">Item:</span>
                                {order.item}
                                </small>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div> 


            <div>

            <nav className="flex "> 
                {
                    nav ? nav.map((item,key)=>(
                    <button onClick={()=>handleSelected(item.id)} 
                    className={`text-gray-800 py-2 px-2.5 block   focus:outline-none ${selectedNav == item.id ? 'border-green-500 bg-white  border-t-2 font-bold ' :'' }`}>
                    
                        {item.name}
                    </button>
                    ))
                    :
                    null
                }  
                </nav>
 
                <div className="flex justify-center ">
                    <div className="bg-white  w-full px-3 py-3">
                        {
                            selectedNav == 1 ?
                          
                            <DeliveryPickupDetails updateStatus={updateStatus} toggleLoading={toggleLoading} type="pickup" reloadData={getData} statusId={order.status_id} data={order.pickup_details} />
                            :
                            <DeliveryPickupDetails  updateStatus={updateStatus} toggleLoading={toggleLoading} type="delivery" reloadData={getData}  statusId={order.status_id} data={order.delivery_details} />

                        } 
                    </div>
                </div>



            </div>
        </div> 
            : 
            
            <Spinner/>
        }
         
      </div>
  );
}

Index.propTypes = {}

export default Index;
 


