import React, { Component } from 'react'

import { Route, Link } from 'react-router-dom'; 

const Breadcrumbs = (props) => (
    <nav className="py-2 px-2 rounded">
      <ol className="list-reset flex text-grey-dark bread-crumbs"> 
            <Route path='/:path' component={BreadcrumbsItem} />
            </ol>
    </nav>
)

const BreadcrumbsItem = ({ match, ...rest }) => (
    <React.Fragment> 
        <li>
            <Link to={match.url || ''} className={`${match.isExact ? 'font-bold text-gray-600' : undefined} mr-2 ml-2 text-sm text-gray-400 hover:text-green-400 hover:underline`}>
                {match.params.path}
            </Link>
        </li>
        <Route path={`${match.url}/:path`} component={BreadcrumbsItem} />
    </React.Fragment>
)
export default Breadcrumbs