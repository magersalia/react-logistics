import React, { useState,Fragment } from 'react';  

import { Disclosure, Menu, Transition } from '@headlessui/react'
import { BellIcon, MenuIcon, XIcon,HomeIcon } from '@heroicons/react/outline'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBoxOpen,faTachometerAlt,faMoneyBillWave,faMotorcycle,faUsersCog } from '@fortawesome/free-solid-svg-icons'


import { NavLink, Link, useHistory } from 'react-router-dom';
  

import Avatar from 'react-avatar';
import Breadcrumbs from './Breadcrumbs';


 function  Nav (){
    let history = useHistory();
    const navigation = [
        { name: 'Dashboard', href: '/admin/dashboard',
          icon:<FontAwesomeIcon className="mr-2" icon={faTachometerAlt} />
      },   
        { name: 'Order Management', href: `/admin/orders`,
        icon:<FontAwesomeIcon className="mr-2" icon={faMoneyBillWave} /> }, 
        { name: 'Delivery Riders', href: `/admin/riders`,
        icon:<FontAwesomeIcon className="mr-2" icon={faMotorcycle} /> }, 
        { name: 'System Users', href: `/admin/users`,
        icon:<FontAwesomeIcon className="mr-2" icon={faUsersCog} /> }, 
      ]
  
      const userNavigation = [
        { name: 'Your Profile', href: '#' },
        { name: 'Settings', href: '#' },
        { name: 'Sign out', href: '/admin/logout'},
      ]
      function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
      }
      const handleLogout = () => { 
        window.location.href = '/admin/logout'; 
      };
        return (
            <div>
            <Disclosure as="nav" className="bg-gray-700 border-gray-200">
            {/* <Disclosure as="nav" className="bg-gray-800"> */}
                   {({ open }) => (
                     <>
                       <div className="px-6 border-b border-gray-200 ">
                         <div className="flex items-center justify-between h-16">
                           <div className="flex items-center">
                             <div className="flex-shrink-0">
                             <FontAwesomeIcon   className="text-green-500  text-2xl mr-2" icon={faBoxOpen} />
                             <strong className="text-green-500 text-2xl font-bold">BENTAHAN </strong>
                             <span className="text-gray-200 font-thin text-shadow">Logistics</span>
                             </div>
                             <div className="hidden md:block">
                               <div className="ml-10 flex items-baseline space-x-4">
                                 {navigation.map((item) => (
                                    <NavLink
                                    key={item.name}
                                    to={item.href}
                                    activeClassName="bg-green-500 text-white shadow-lg  px-3 py-2 rounded-md text-sm font-medium"
                                    className={'text-white'
                                    } >
                                      {item.icon}
                                     {item.name}
           
                                  </NavLink> 
                                 ))}
                               </div>
                             </div>
                           </div>
                           <div className="hidden md:block">
                             <div className="ml-4 flex items-center md:ml-6">
                               <button
                                 type="button"
                                 className="bg-white p-1 rounded-full text-white shadow-lg hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                               >
                                 <span className="sr-only">View notifications</span>
                                 <BellIcon className="h-6 w-6 text-gray-500" aria-hidden="true" />
                               </button>
           
                               {/* Profile dropdown */}
                               <Menu as="div" className="ml-3 relative">
                                 <div>
                                   <Menu.Button className="max-w-xs bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                     <span className="sr-only">Open user menu</span> 
                                     <Avatar 
                             className=" shadow-lg"
                                     src={user.avatar}   
                                     name={user.name}  
                                      size={40} round="100%" />
                                   </Menu.Button>
                                    
                                 </div>
                                 <Transition
                                   as={Fragment}
                                   enter="transition ease-out duration-100"
                                   enterFrom="transform opacity-0 scale-95"
                                   enterTo="transform opacity-100 scale-100"
                                   leave="transition ease-in duration-75"
                                   leaveFrom="transform opacity-100 scale-100"
                                   leaveTo="transform opacity-0 scale-95"
                                 >
                                   <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow  py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                                     {userNavigation.map((item) => (
                                       <Menu.Item key={item.name}>
                                         {({ active }) => (
                                           <a
                                             href={item.href}
                                             className={classNames(
                                               active ? 'bg-gray-100' : '',
                                               'block px-4 py-2 text-sm text-gray-700'
                                             )}
                                             onClick=
                                             {
                                               item.name == 'Sign out' ? handleLogout : ''
                                             }
                                           >
                                             {item.name}
                                           </a>
                                         )}
                                       </Menu.Item>
                                     ))}
                                   </Menu.Items>
                                 </Transition>
                               </Menu>
                             </div>
                           </div>
                           <div className="-mr-2 flex md:hidden">
                             {/* Mobile menu button */}
                             <Disclosure.Button className="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                               <span className="sr-only">Open main menu</span>
                               {open ? (
                                 <XIcon className="block h-6 w-6" aria-hidden="true" />
                               ) : (
                                 <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                               )}
                             </Disclosure.Button>
                           </div>
                         </div>
                       </div>
           
                       <Disclosure.Panel className="md:hidden">
                         <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                           {navigation.map((item) => (
                            <NavLink
                            key={item.name}
                            to={item.href}
                            activeClassName="bg-green-500 text-white shadow-lg  px-3 py-2 rounded-md text-sm font-medium"
                            className={'text-white block mb-2'} >
                              {item.icon}
                             {item.name}
   
                          </NavLink> 
                             
                           ))}
                         </div>
                         <div className="pt-4 pb-3 border-t border-gray-700">
                           <div className="flex items-center px-5">
                             <div className="flex-shrink-0"> 
                                     <Avatar 
                             className=" shadow-lg"
                                     src={user.avatar}   
                                     name={user.name}  
                                      size={40} round="100%" />
                             </div>
                             <div className="ml-3">
                               <div className="text-base font-medium leading-none text-white">{user.name}</div>
                               <div className="text-sm font-medium leading-none text-gray-400">{user.email}</div>
                             </div>
                             <button
                               type="button"
                               className="ml-auto bg-white flex-shrink-0 p-1 rounded-full text-gray-800 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                             >
                               <span className="sr-only">View notifications</span>
                               <BellIcon className="h-6 w-6" aria-hidden="true" />
                             </button>
                           </div>
                           <div className="mt-3 px-2 space-y-1">
                             {userNavigation.map((item) => (
                               <a
                                 key={item.name}
                                 href={item.href}
                                 className="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                               >
                                 {item.name}
                               </a>
                             ))}
                           </div>
                         </div>
                       </Disclosure.Panel>
                     </>
                   )}
                 </Disclosure> 
                   
                 <div className="bg-white  shadow">
                   <div className="px-6">  
                     <Breadcrumbs/>  
                   </div>
                 </div>
                 </div>
                 
        ) 
}


export default Nav