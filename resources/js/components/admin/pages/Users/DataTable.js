import React,{Fragment,useEffect} from 'react'; 

import { Disclosure, Menu, Transition } from '@headlessui/react'
import { TrashIcon,EyeIcon,PencilIcon } from '@heroicons/react/solid'
 
import { useHistory } from 'react-router-dom';

import Avatar from 'react-avatar';
import moment from 'moment';
import Loader from '../../../rider/pages/Loader';
function DataTable (props) { 

  
  const deleteRecord = (data) =>{
    let url = '/api/admin/users/delete/'+data.id; 
    axios.get(url)
    .then(response => {  
      props.reloadData && props.reloadData()
      props.toast && props.toast(`User "${data.name}" has been successfully removed`)

    })
    .catch(error => { 
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        } 
    }); 
  }

    const {heading,data} = props;  
    let history = useHistory(); 
    return (
        // <div>
        //     <table className="table-auto table min-w-full divide-y divide-gray-200">
        //     <thead className="bg-gray-100">
        //         <tr>
        //         {
        //             heading ?
        //                 heading.map((val)=>
        //                     <th className="text-gray-700 uppercase tracking-wider">{val.name}</th>
        //                 ) 
        //             : null
        //         } 
        //         </tr>
        //     </thead>
        //     <tbody className="bg-white divide-y divide-gray-200">
        //     {
        //             data ?
        //             data.map((item, index) =>
        //                 <tr key={item.id}>
        //                     <th className="px-6 py-4 text-left whitespace-nowrap">{item.name}</th>
        //                 </tr>
        //                 ) 
        //             : null
        //         }  
        //     </tbody>
        //     </table>
        // </div>
      
        <div className="flex flex-col">
     <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y  border border-gray-100 divide-gray-200"> 
            <thead className="bg-gray-100  border-gray-200">  
                 <tr> 
                  <th>#</th>
      {
                    heading ?
                        heading.map((val)=> 
                        <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-900 uppercase tracking-wider"
                      >{val.name}</th>
                        ) 
                    : null
                } 
                </tr> 
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {data  ? data.map((item,key) => (
                  <tr key={key} className="hover:bg-gray-100 hover:border-b-8 cursor-pointer">
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500  border-b " width="25">{key + 1}</td>
                    <td className="px-6 py-4 whitespace-nowrap border-b ">
                      <div className="flex items-center">
                        <div className="flex-shrink-0 h-10 w-10">
                        <Avatar 
                        className=" shadow "
                        src={item.avatar}
                        name={item.name}
                        size={40} round="100%" />
                         </div>
                        <div className="ml-4">
                          <div className="text-sm font-medium text-gray-900">{item.name}</div>
                          <div className="text-sm text-gray-500">{item.email}
                           
                          </div>
                        </div>
                      </div>
                    </td> 
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500  border-b ">{item.role.name}</td>
                    <td className="px-6 py-4 whitespace-nowrap  border-b ">
                      <div className="text-sm text-gray-900">{moment(item.created_at).format('MM/DD/YYYY hh:mm A')}</div> 
                    </td> 
                    <td className="px-6 py-4 whitespace-nowrap  border-b ">
                      <div className="bg-green-400 text-white px-0 py-1 text-xs  rounded-full  text-center">
                        Active
                      </div> 
                    </td> 
                    <td className="px-6 py-4 whitespace-nowrap text-right  border-b  text-sm font-medium">
                    <button onClick={()=>alert('HEY')} className={`bg-green-500 hover:bg-green-700 text-white    py-2 px-2 rounded mr-2`}>
                    <EyeIcon className="h-4 w-4" aria-hidden="true" /> 
                    </button>  
                    <button onClick={()=>
                    history.push(`/admin/users/edit/${item.id}`)
                    } className={`bg-green-500 hover:bg-green-700 text-white    py-2 px-2 rounded mr-2`}>
                    <PencilIcon className="h-4 w-4" aria-hidden="true" /> 
                    </button>   
                    <button onClick={()=>{
                       deleteRecord(item)
                    }} className={`bg-red-600 hover:bg-red-700 text-white    py-2 px-2 rounded`}>
                    <TrashIcon className="h-4 w-4" aria-hidden="true" />
                    </button> 
                    </td>
                  </tr>
                )): 
                    <Loader/>
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    )
}


export default DataTable;