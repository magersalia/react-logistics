<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
 

    <!-- Fonts --> 

    <!-- Styles --> 
    <link rel="stylesheet" href="{{asset('/font/css/all.css')}}">

    <link href="{{asset('anchor/assets/css/main.css')}}" rel="stylesheet"/>
    <link href="{{asset('anchor/assets/css/vendor/aos.css')}}" rel="stylesheet"/> 

    <style>

ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
.loader {
  border: 10px solid #eaeaea; /* Light grey */
  border-top: 10px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 50px;
  height: 50px;
  animation: spin 0.9s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
 
    </style>
</head>
<body class="bg-light" style="margin-bottom: 50px">
    <div id="app">  
        {{-- <div class="container-fluid pt-4"> --}}
            
            
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
            @yield('content')
        {{-- </div> --}}
    </div>
<script src="{{asset('anchor/assets/js/vendor/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/vendor/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/vendor/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/functions.js')}}" type="text/javascript"></script>

<script src="{{asset('anchor/assets/js/vendor/aos.js')}}" type="text/javascript"></script>
@stack('js')   

</body>
</html>
