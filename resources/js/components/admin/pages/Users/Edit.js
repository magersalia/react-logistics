import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
 
import CreateUpdateForm from './CreateUpdateForm';
import { PlusIcon,ArrowLeftIcon } from '@heroicons/react/solid'


function Index (props) {


  let [user,setUser] = useState([]);

  let [loading,setLoading] = useState(false);
 
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
 

  const getData = () =>{
    let id = props.match.params.id,
        url = `/api/admin/users/edit/${id}`;
        axios.get(url)
        .then(response => {  
          setUser(response.data.data)    
        })
        .catch(error => { 
            if (error.response.status == 422) {
                // setFormErrors(error.response.data.errors)
            } 
        });
  }
  useEffect(() => { 
    getData() 
}, []);

 
  
  return ( 
      <div>
        {
          loading ?
            <div class="w-full h-full fixed block top-0 left-0 bg-gray-800 opacity-75">
              <span class="text-green-500 z-99 top-1/2 my-0 mx-auto block relative w-0 h-0"
              style={{top: '50%'}}>
              <div class="loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-10 w-10"></div>
              </span>
            </div>
            :
            ''
        }
        
        <header className="bg-white overflow-hidden  mb-4 ">
        <div className="  mx-auto py-3   sm:px-6 lg:px-1 grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500 mt-2">
        <Link to='/admin/users'>
        <ArrowLeftIcon className="inline h-6 w-6 pb-1 mr-4" area-hidden="true" />
        </Link> 
          Update {user.name || ''}
          </h1>
        </div> 
        </div>
      </header>

        <CreateUpdateForm data={user} editMode={true} />
      
      </div>
  );
}

Index.propTypes = {}

export default Index;
 


