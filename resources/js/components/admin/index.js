import React from 'react';
import ReactDOM from 'react-dom'; 
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import routes from './route-list'
import Nav from './pages/Nav'
import NotFound from './pages/NotFound'
import AuthRoute from './auth-route' 
import TopBarLoader from './TopBarLoader'
 
export default function App() {
 
  return (   
    <Router> 
    <Nav/> 
        <div className="container-fluid my-3">
          <TopBarLoader />
          <Switch>
            {
              routes ? routes.map((item)=>(
                <AuthRoute exact={item.exact} path={item.path} component={item.component} title={item.title} /> 
              )): null
            }
            
            <Route component={NotFound} className="bg-gray"/>
          </Switch>
        </div> 
    </Router>
  );
}
 
if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
