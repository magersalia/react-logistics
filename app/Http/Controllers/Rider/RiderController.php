<?php

namespace App\Http\Controllers\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    //
    public function index(){
        if(\Auth::check()){
            return view('rider-app');
        }else{
            return redirect('login');
        }
    }
}
