<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function index(){
        if(\Auth::check()){
            $role = \Auth::user()->role_id;
            if($role == 1 || $role ==2 ){
                // Authentication passed...
                return view('app');
            }else{
                \Auth::logout();
                return redirect('admin/login')->withErrors(['message'=>'Login credentials not found']);
            }
        }else{
            return redirect('/admin/login');
        }
    }
}
