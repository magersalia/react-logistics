<?php

use Illuminate\Support\Facades\Route;
use App\Order;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view("welcome");
});

Route::group(['prefix'=>'admin','namespace'=>'Admin'],function(){
    Route::get("/logout",'LoginController@logout'); 
    Route::get("/login",'LoginController@index');
    Route::post("/login/authenticate",'LoginController@authenticate');
 
    Route::get('/{uri?}','AdminController@index')->where('uri', '(.*)'); 
});


Route::group(['namespace'=>'Rider'],function(){
    
    Route::get("/logout",'LoginController@logout'); 
    Route::get("/login",'LoginController@index');
    Route::post("/login/authenticate",'LoginController@authenticate');

    Route::group(['prefix'=>'rider'],function(){
        Route::get('/{uri?}','RiderController@index')->where('uri', '(.*)'); 
    });
});



Route::get("/test",function(Illuminate\Http\Request $request){
      
    // $order->order_id = $request->order_id;
    // $order->tracking_no = $request->tracking_no;
    // $order->size = $request->size; 
    // $order->weight = $request->weight; 
    // $order->total = $request->total; 
    // $order->bentahan_order_id = $request->bentahan_order_id ?? null;  
    // $order->save();

}); 

