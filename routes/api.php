<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'orders'],function(){
    Route::get('/tracker','OrderController@track');
});

Route::namespace('Rider')->group(function () {
    Route::namespace('Api')->group(function () {
        Route::group(['prefix'=>'rider'],function(){ 

            Route::group(['prefix'=>'orders'],function(){ 
                Route::get('/{id}','OrderController@getDataForDashboard'); 
                Route::get('/details/{id}','OrderController@details'); 
                Route::get('/update-status/{id}/{status}','OrderController@updateStatus'); 

                

                Route::group(['prefix' => 'pickup'], function () {  
                    Route::get('/details/{order_id}','PickupDetailsController@details'); 
                    Route::get('/update/{id}/{status}','PickupDetailsController@update')->name('pickup.update'); 
                    Route::post('/cancel/{pickup_detail}','PickupDetailsController@cancel')->name('pickup.cancel'); 
                });

                Route::group(['prefix' => 'delivery'], function () { 
                    Route::post('/cancel/{delivery_detail}','DeliveryDetailsController@cancel')->name('deliver.cancel'); 
                    Route::get('/update/{id}/{status}','DeliveryDetailsController@update')->name('deliver.update'); 
                });


                });
            });
    });
});
Route::namespace('Admin')->group(function () {
    Route::namespace('Api')->group(function () {
        Route::group(['prefix'=>'admin'],function(){
            Route::group(['prefix'=>'users'],function(){
                Route::get('/','UserController@index');
                Route::post('/create','UserController@create');
                Route::get('/edit/{id}','UserController@edit');
                Route::get('/delete/{id}','UserController@delete');
                Route::post('/update/{id}','UserController@update');
            });

            Route::group(['prefix'=>'riders'],function(){
                Route::get('/','RiderController@index');
                Route::post('/create','RiderController@create');
                Route::get('/edit/{id}','RiderController@edit');
                Route::get('/delete/{id}','UserController@delete');
                Route::post('/update/{id}','RiderController@update');
            });

            Route::group(['prefix'=>'orders'],function(){
                Route::get('/','OrderController@index');
                Route::any('/create','OrderController@create');
                Route::get('/edit/{id}','OrderController@edit');
                Route::get('/delete/{id}','OrderController@delete');
                Route::any('/update/{id}','OrderController@update');
                Route::post('/getPickupDetails/{id}','OrderController@getPickupDetails');
                Route::post('/getDeliveryDetails/{id}','OrderController@getDeliveryDetails');
            });

        });
    });
});