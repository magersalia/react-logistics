import React,{useState,useEffect} from 'react'; 
 
function Index (props) {
    return ( 
        <div className="w-full h-full absolute top-0 left-0 bg-gray-300 opacity-75" style={{zIndex:'99999999999999999999999999999999999'}}>
        <span className="bg-green-500  z-99 top-1/2 my-0 mx-auto block relative w-0 h-0"
        style={{top: '50%'}}>
        <div className="loader  ease-linear rounded-full border-8 border-t-8 border-gray-200 h-10 w-10"></div>
        </span>
      </div> 
    )
}
 

export default Index;