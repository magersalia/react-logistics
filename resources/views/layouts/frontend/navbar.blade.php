<nav class="navbar navbar-expand-lg shadow-sm navbar-light bg-white">
<div class="container">
	<a class="navbar-brand " href="#"><i class="fas fa-box-open text-success mr-2 "></i><strong class="text-success">Bentahan </strong>Logistics
	</a>
        
        
        	<ul class="navbar-nav mr-auto">
               
            </ul>
			@auth
		<ul class="navbar-nav ">	
            <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="collapse" href="#collapseExample" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="rounded-circle" src="{{Voyager::image(auth()->user()->avatar)}}" width="30">
				</a>
			</li>
		</ul> 
		@endauth
</div>
</nav> 


<div class="collapse  mx-auto shadow" id="collapseExample"> 
             <ul class="list-group">
				 <a href="" class="list-group-item text-dark"><i class="fa fa-truck mr-2"></i>My Deliveries</a>
				 <a href="" class="list-group-item text-dark"><i class="fa fa-user mr-2"></i>My Account</a> 
				 <a href="{{ url('logout') }}"  class="list-group-item text-dark"
				 onclick="event.preventDefault();
						  document.getElementById('logout-form').submit();">
						  <i class="far fa-signout mr-2"></i>
				 Log out
				 </a>
			 </ul>
			 
			 <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
</div>

