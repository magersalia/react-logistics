import React,{useState,useEffect} from 'react';
import { NavLink, Link, useHistory } from 'react-router-dom';
// import Spinner from './Spinner';
import Spinner from '../../elements/Spinner'
import Nav from '../pages/Nav'
function Dashboard() {
    let [orders,setOrders] = useState(null);  
    let [dataCount,setDataCount] = useState(null);  
    let [selectedNav,setSelectedNav] = useState(null);  
    const nav = [
        
        {
            name:'All',
            count:orders?dataCount.all:0,
            id:null
        },
        {
            name:'Active',
            count:dataCount?dataCount.active:0,
            id:9
        },
        {
            name:'Pending',
            count:dataCount?dataCount.pending:0,
            id:3
        },
        {
            name:'Completed',
            count:dataCount?dataCount.completed:0,
            id:11
        },
        {
            name:'Cancelled',
            count:dataCount?dataCount.cancelled:0,
            id:12
        },
    ]
  const getData =(id=null)=>{
    let url = `/api/rider/orders/${user.id}?status=${id}` ; 

  
    axios.get(url)
    .then(response => {  
          
        setDataCount(response.data.data.nav) 
        setOrders(response.data.data.orders)  
    })
    .catch(error => {
        // setisLoading(false)
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        }
        // Swal.fire("Something went wrong!", "", "error")
    }); 
  }
  
    useEffect(() => { 
        getData(null)  
    }, []);

    const handleSelected = (id)=>{
        setSelectedNav(id) 
        getData(id)
    }

    const listClass = (status) =>{ 
        let display = '';
        switch (status) {
            case 'Completed':
                 display = 'border-l-8 border-green-500'
            break;
            case 'Accepted':
                 display = 'border-l-8 border-blue-300'
            break;
            case 'Pending': 
                display = 'border-l-8 border-yellow-500'
            break;
            case 'Cancelled': 
                display = 'border-l-8 border-red-500'
            break; 
        }

        return display;
    }

    
    const badgeClass = (status) =>{ 
        let display = '';
        switch (status) {
            case 'Completed':
                 display = 'bg-green-500'
            break;
            case 'Accepted':
                 display = 'bg-blue-300'
            break;
            case 'Pending': 
                display = 'bg-yellow-500'
            break;
            case 'Cancelled': 
                display = 'bg-red-500'
            break; 
        }

        return display;
    }

    return (
        <div className="mb-5"> 
            <Nav/> 
            <div className="px-2 mt-2"> 
            <h1 className="mb-3">Orders Assigned to you</h1>
            
            {
                orders ?
                <>
                <nav className="flex "> 
                {
                    nav ? nav.map((item,key)=>(
                    <button onClick={()=>handleSelected(item.id)} 
                    className={`text-gray-600  py-2 px-2.5 text-xs block hover:text-blue-500 focus:outline-none ${selectedNav == item.id ? 'border-green-500 bg-white text-green-500 border-t-2 font-bold ' :'' }`}>
                        {item.count}<br></br>
                        {item.name}
                    </button>
                    ))
                    :
                    null
                }  
                </nav>

                <div className="font-sans flex items-center justify-center w-full">
                <div className="overflow-hidden bg-white rounded w-full  leading-normal">
                    { 
                        orders.length >= 1 ?
                        orders.map((item,key)=>(
                            <Link to={`/rider/order/${item.id}`} className={`block group px-2 py-4 ${listClass(item.status.name)} divide-x` }>
                                <p className="font-bold text-black ">{item.order_id}</p>
                                {
                                    item.status.name == 'Accepted' || item.status.name == 'Completed' ?
                                    <div className="mt-1">
                                    <span for="" className="block"><span className='text-gray-700'>Started on:</span>  {item.start_date}</span>
                                    <span for="" className="block"><span className='text-gray-700'>Duration: </span> {item.duration}</span> 
                                    </div>
                                    :
                                    null
                                }  
                                <span className={`rounded-full px-1 py-1 text-white mb-2 text-xs ${badgeClass(item.status.name)} `}>{item.status.name} </span>
                                                                
                            </Link>
                        ))
                        :
                        <a href="#" className="block group p-4 border-b">
                            <p className="font-bold text-lg mb-1 text-black ">No Data Found</p>
                            <p className="text-grey-darker mb-2 "> 
                            </p>
                        </a>    
                    }
                    
                </div>
                </div> 
                </>

                :
                <Spinner/>
            }
           
            </div> 

        </div>
    ) 
}

export default Dashboard
