<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    //
    protected $fillable = ['contact_person','contact_no','address','order_id'];
    
    public function order(){
        return $this->belongsTo('App\Order','id','order_id');
    }
}
