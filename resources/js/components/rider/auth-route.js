import React from 'react';
import { Route, Redirect } from 'react-router-dom'; 
import PropTypes from 'prop-types'; 
function AuthRoute ({ component: Component, title, ...rest }) { 
  return ( 
    <Route
    {...rest}
    render={props => 
      {
        return (

          <div className="bg-gray-100">
            <div className="flex flex-col min-h-screen lg:w-1/2 mx-auto">
            <Component {...props} />
          </div> 
        </div> 
        )
      } 
    }
  />
  );
};

AuthRoute.displayName = 'Auth Route';

AuthRoute.propTypes = {
  component: PropTypes.func.isRequired,
  rest: PropTypes.object,
  location: PropTypes.object,
  title: PropTypes.string
};

export default AuthRoute;
