import React, { Component } from 'react'
import { NavLink, Link, useHistory } from 'react-router-dom';

  
const navigation = [
    { name: 'Dashboard', href: '/admin/dashboard'},   
    { name: 'Order Management', href: `/admin/orders` }, 
    { name: 'Delivery Riders', href: `/admin/riders` }, 
    { name: 'System Users', href: `/admin/users` }, 
  ]
 function  Nav (){
 
        return (
            <div>
          <nav className="navbar navbar-expand-lg navbar-light bg-white border-top border-success" style={{borderTopWidth:"10px"}}>
            <a className="navbar-brand" href="#"><i className="glyphicon glyphicon-home"></i> <strong className="text-success">Bentahan</strong> Logistics</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                {/* <li className="nav-item active">
                    <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">Link</a>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a className="dropdown-item" href="#">Action</a>
                    <a className="dropdown-item" href="#">Another action</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li className="nav-item">
                    <a className="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li> */}
                {navigation.map((item) => (
                         <NavLink
                         key={item.name}
                         to={item.href}
                         activeClassName="bg-success text-white  px-3 py-2 rounded"
                         className={' hover:border-b-4 hover:border-green-500 px-3 py-2 rounded-md text-sm font-medium'
                         } >
                           {item.icon}
                          {item.name}

                       </NavLink> 
                      ))}
                </ul>
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a className="dropdown-item" href="#">Action</a>
                    <a className="dropdown-item" href="#">Another action</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                </ul>
                {/* <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> */}
            </div>
            </nav>
            <div className="container-fluid bg-white py-2 border-top shadow-sm">
                aaa {'>'} bbbb
            </div>
            </div>
        ) 
}


export default Nav