import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
 
import DataTable from './DataTable';
import { PlusIcon, CloudUploadIcon } from '@heroicons/react/outline'

import Spinner from '../../../elements/Spinner'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Index (props) { 
  
  let {data,editMode,id} = props; 
  let history = useHistory();

  let [orders,setOrders] = useState([]);
  let [pickupDetails,setPickupDetails] = useState({});
  let [deliveryDetails,setDeliveryDetails] = useState({});
  let [avatar,setAvatar] = useState({});
  let [loading,setLoading] = useState(false);  
  let [formErrors,setFormErrors] = useState({})
   
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
  const resolveAfter3Sec = new Promise(resolve => setTimeout(resolve, 2000));
 
  const notify = (message) =>{
    
toast.promise(
    resolveAfter3Sec,
    {
      pending: {
        render(){
          return "Saving data ..."
        },
        icon: true,
      },
      success: {
        render({data}){
          // return `Data successfully saved`
          history.push('/admin/orders',{withSuccess:true})
        },
        // other options
        icon: true,
      },
      error: {
        render({data}){
          // When the promise reject, data will contains the error
          return <MyErrorComponent message={data.message} />
        }
      }
    }
)
  };

  
  const cancelChanges = (e) =>{

  }
  const saveData = (e) =>{
    // notify('Data successfully saved')

    e.preventDefault(); 
    let url = '';
    if(!editMode){
      url = `/api/admin/orders/create` ;  
    }else{
      url = `/api/admin/orders/update/${data.id}` ; 
    } 

    let formData = new FormData(); 
    for (const key in orders) {
      if (orders.hasOwnProperty(key)) {
          const item = orders[key]; 
          formData.append(key, item)
      }
  }
  for (const pickup_key in pickupDetails) {
    if (pickupDetails.hasOwnProperty(pickup_key)) {
        const item = pickupDetails[pickup_key]; 
        formData.append(`pickup_details[${pickup_key}]`, item)
    }
  } 
  
  for (const delivery_key in deliveryDetails) {
    if (deliveryDetails.hasOwnProperty(delivery_key)) {
        const item = deliveryDetails[delivery_key]; 
        formData.append(`delivery_details[${delivery_key}]`, item)
    }
  } 
     
    axios.post(url,formData)
    .then(response => {  
        if(response.data.success){
            setOrders({}) 
            setPickupDetails({}) 
            setDeliveryDetails({}) 
            setFormErrors({}) 
            history.push('/admin/orders')  

            // notify
        }
    })
    .catch(error => {
        // setisLoading(false)
        if (error.response.status == 422) {
            setFormErrors(error.response.data.errors)
        } 
    });
    
  }

  const handleChange = (event) =>{
    let inputData = {...orders};
    inputData[event.target.name] = event.target.value; 
    setOrders(inputData);
}

const handlePickupDetailChanges = (event) =>{
  let inputData = {...pickupDetails || props.data.pickup_details};
  inputData[event.target.name] = event.target.value;   
  setPickupDetails(inputData);
}

const handleDeliveryDetailChanges = (event) =>{
  let inputData = {...deliveryDetails || props.data.delivery_details};
  inputData[event.target.name] = event.target.value; 
   
  setDeliveryDetails(inputData);
}

const invalid = (field) =>{
  return formErrors[field]  ? true : false;
}
 


  useEffect(() => {   
}, []);

const handleFileChange = (event) => { 
  setAvatar(event.target.files[0]);
}

  
  return ( 
      <div>
      {
        
        loading ?
            <Spinner/>
          :
          '' 
      }       
       <ToastContainer 
                       position="top-right"
                       autoClose={5000}
                       hideProgressBar={false}
                       newestOnTop={false}
                       closeOnClick
                       rtl={false}
                       pauseOnFocusLoss
                       draggable
                       pauseOnHover
                       theme="colored"
                       /> 

      <div class="grid grid-cols-2 gap-6 mb-5"> 
        <div className="bg-white w-full border rounded mb-4 ">  
              <div className="px-4 bg-green-500  border-none rounded-t text-white py-4 ">
                  <h3 className="font-bold  uppercase">Pickup Details</h3>
              </div>
              <div className=" px-4 pt-2 pb-4 ">

              <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Contact Person :</label>
                    <input onChange={handlePickupDetailChanges} value={editMode ? (pickupDetails.contact_person ? pickupDetails.contact_person : (data.pickup_details ? data.pickup_details.contact_person : '')) : pickupDetails.contact_person} 
                    className={`border ${invalid('pickup_details.contact_person') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="contact_person" />
                    {
                      formErrors['pickup_details.contact_person'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['pickup_details.contact_person']
                        }
                      </span>
                      :null
                    }
              </div> 
                <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Contact No: </label>
                    <input onChange={handlePickupDetailChanges} value={editMode ? (pickupDetails.contact_no ? pickupDetails.contact_no : (data.pickup_details ? data.pickup_details.contact_no : '')) : pickupDetails.contact_no} 
                    className={`border ${invalid('pickup_details.contact_no') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="contact_no" />
                    {
                      formErrors['pickup_details.contact_no'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['pickup_details.contact_no']
                        }
                      </span>
                      :null
                    }
                </div> 
                <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Address: </label>
                    <textarea 
                    className={`border ${invalid('pickup_details.address') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="address" onChange={handlePickupDetailChanges} value={editMode ? (pickupDetails.address ? pickupDetails.address : (data.pickup_details ? data.pickup_details.address : '')) : pickupDetails.address}   ></textarea>
                    {
                      formErrors['pickup_details.address'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['pickup_details.address']
                        }
                      </span>
                      :null
                    }
                  </div>  
              </div>
        </div> 
        <div className="bg-white w-full border rounded mb-4 ">  
              <div className="px-4 py-4 bg-yellow-500   rounded-t text-white ">
                  <h3 className="font-bold  uppercase">Delivery Details</h3>
              </div>
              <div className=" px-4 pt-2 pb-4 ">

              <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Contact Person :</label>
                    <input onChange={handleDeliveryDetailChanges} value={editMode ? (deliveryDetails.contact_person ? deliveryDetails.contact_person : (data.delivery_details ? data.delivery_details.contact_person : '')) : deliveryDetails.contact_person}   
                    className={`border ${invalid('delivery_details.contact_person') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="contact_person" />
                    {
                      formErrors['delivery_details.contact_person'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['delivery_details.contact_person']
                        }
                      </span>
                      :null
                    }
                </div> 
                <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Contact No: </label>
                    <input onChange={handleDeliveryDetailChanges} value={editMode ? (deliveryDetails.contact_no ? deliveryDetails.contact_no : (data.delivery_details ? data.delivery_details.contact_no : '')) : deliveryDetails.contact_no} 
                    className={`border ${invalid('delivery_details.contact_no') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="contact_no" />
                    {
                      formErrors['delivery_details.contact_no'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['delivery_details.contact_no']
                        }
                      </span>
                      :null
                    }
                </div> 
                <div className="form-group "> 
                    <label className="text-gray-800 text-xs font-bold">Address: </label>
                    <textarea value={editMode ? (deliveryDetails.address ? deliveryDetails.address : (data.delivery_details ? data.delivery_details.address : '')) : deliveryDetails.address}
                    className={`border ${invalid('delivery_details.address') ? 'border-red-700' : 'border-gray-400'}  rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="address" onChange={handleDeliveryDetailChanges}>
                      </textarea>
                    {
                      formErrors['delivery_details.address'] ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors['delivery_details.address']
                        }
                      </span>
                      :null
                    }
                  </div>  
              </div>
        </div> 

      </div>
        <div className="bg-white  border rounded"> 
        <div className="px-4 bg-gray-100 text-gray-800   rounded-t  py-4 ">
                  <h3 className="font-bold  uppercase">Order Details 
                </h3>
              </div>

            <div className="px-4 pt-2 pb-4"> 
                <div className="form-group mt-2"> 
                    <label className="text-gray-800 text-xs font-bold">Order ID :</label>
                    <input onChange={handleChange} value={editMode ? (orders.order_id ? orders.order_id : data.order_id) : orders.order_id} 
                    className={`border ${invalid('order_id') ? 'border-red-700' : 'border-gray-400'} rounded d-inline mt-2   px-4 py-2 w-full`}
                    name="order_id" />
                    {
                      formErrors.order_id ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors.order_id
                        }
                      </span>
                      :null
                    }
                </div>
                <div className="form-group mt-2">
                    <label className="text-gray-800 text-xs font-bold">Tracking No :</label>
                    <input onChange={handleChange} value={editMode ? (orders.tracking_no ? orders.tracking_no : data.tracking_no) : orders.tracking_no}  
                    className={`border ${invalid('tracking_no') ? 'border-red-700' : 'border-gray-400'} rounded d-inline mt-2   px-4 py-2 w-full`}
                    type="text" name="tracking_no" />
                    {
                      formErrors.tracking_no ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors.tracking_no
                        }
                      </span>
                      :null
                    }
                </div>
                <div className="form-group mt-2">
                    <label className="text-gray-800 text-xs font-bold">Size :</label>
                    <input onChange={handleChange} value={editMode ? (orders.size ? orders.size : data.size) : orders.size}  
                    className={`border ${invalid('size') ? 'border-red-700' : 'border-gray-400'} rounded d-inline mt-2   px-4 py-2 w-full`}
                    type="text" name="size" />
                    {
                      formErrors.size ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors.size
                        }
                      </span>
                      :null
                    }
                </div> 
                <div className="form-group mt-2">
                    <label className="text-gray-800 text-xs font-bold">Weight :</label>
                    <input onChange={handleChange} value={editMode ? (orders.weight ? orders.weight : data.weight) : orders.weight}  
                    className={`border ${invalid('weight') ? 'border-red-700' : 'border-gray-400'} rounded d-inline mt-2   px-4 py-2 w-full`}
                    type="text" name="weight" />
                    {
                      formErrors.weight ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors.weight
                        }
                      </span>
                      :null
                    }
                </div> 
                <div className="form-group mt-2">
                    <label className="text-gray-800 text-xs font-bold">Total :</label>
                    <input onChange={handleChange} value={editMode ? (orders.total ? orders.total : data.total) : orders.total}  
                    className={`border ${invalid('total') ? 'border-red-700' : 'border-gray-400'} rounded d-inline mt-2   px-4 py-2 w-full`}
                    type="text" name="total" />
                     {
                      formErrors.total ? 
                      <span class="text-xs text-red-700" id="passwordHelp">
                        {
                          formErrors.total
                        }
                      </span>
                      :null
                    }
                </div>
            </div>
        </div>

        <div className="bg-white px-4 py-4 mt-5 rounded text-center"> 
                    <button type="submit"  onClick={saveData}  className="bg-green-500 border hover:bg-green-700 text-white  py-3 px-10 rounded shadow  rounded-full pull-right mr-2">Submit</button> 
        </div>
      </div>
  );
}

Index.propTypes = {}

export default Index;

