import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
 
import DataTable from './DataTable';
import { PlusIcon, CloudUploadIcon } from '@heroicons/react/outline'

import Spinner from '../../../elements/Spinner'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
function Index (props) { 
  
  let {data,editMode} = props; 
  let history = useHistory();

  let [user,setUser] = useState([]);
  let [avatar,setAvatar] = useState({});
  let [loading,setLoading] = useState(false); 

  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
  

  const resolveAfter3Sec = new Promise(resolve => setTimeout(resolve, 3000));
 
  const notify = (message) =>{
    
toast.promise(
    resolveAfter3Sec,
    {
      pending: {
        render(){
          return "Saving data ..."
        },
        icon: true,
      },
      success: {
        render({data}){
          // return `Data successfully saved`
          history.push('/admin/users',{withSuccess:true})
        },
        // other options
        icon: true,
      },
      error: {
        render({data}){
          // When the promise reject, data will contains the error
          return <MyErrorComponent message={data.message} />
        }
      }
    }
)
  };

  
  const saveData = (e) =>{
    e.preventDefault(); 
    let url = '';
    if(!editMode){
      url = `/api/admin/users/create` ;  
    }else{
      url = `/api/admin/users/update/${data.id}` ; 
    } 

    let formData = new FormData();

    
    for (const key in user) {
      if (user.hasOwnProperty(key)) {
          const item = user[key]; 
          formData.append(key, item)
      }
  } 
 
    formData.append('avatar', avatar)  

        
    axios.post(url,formData)
    .then(response => {  
        if(response.data.success){ 
            setUser({})
            setAvatar({})
            
            notify()
        }
    })
    .catch(error => {
        // setisLoading(false)
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        }
        // Swal.fire("Something went wrong!", "", "error")
    });
    
  }

  const handleChange = (event) =>{
      let inputData = {...user};
      inputData[event.target.name] = event.target.value; 
      setUser(inputData);
  }
  useEffect(() => { 
    // getData()   
    setUser(props.data ? props.data : {})
}, []);

const handleFileChange = (event) => { 
  setAvatar(event.target.files[0]);
}

  
  return ( 
      <div>
      {
        
        loading ?
            <Spinner/>
          :
          '' 
      } 
      
      <ToastContainer 
                       position="top-right"
                       autoClose={5000}
                       hideProgressBar={false}
                       newestOnTop={false}
                       closeOnClick
                       rtl={false}
                       pauseOnFocusLoss
                       draggable
                       pauseOnHover
                       theme="colored"
                       /> 
        <div className="bg-white px-4 py-4 shadow"> 
            <form onSubmit={saveData}>
                <div className="form-group mb-5 mt-2" style={{width:'25%'}}> 
                    {
                      editMode ? 
                        <img src={user.avatar ? user.avatar : data.avatar} className="mb-4 flex" />
                      :

                      ''
                    }
                    <label className="bg-green-400 hover:bg-green-500 text-white cursor-pointer block  text-center  rounded px-2 py-2">
                <CloudUploadIcon className="h-6 w-6 mx-auto" aria-hidden="true" />                    
                    Upload Avatar
                    <input type="file" onChange={handleFileChange} style={{display:'none'}} />                    
                    </label>
                    {/* <input onChange={handleChange} value={editMode ? (user.name ? user.name : data.name) : user.name} className="border border-gray-400 rounded d-inline mt-2   px-4 py-2 w-full" name="name" /> */}
                </div>
                <div className="form-group mt-2"> 
                    <label className="text-gray-700">Name :</label>
                    <input onChange={handleChange} value={editMode ? (user.name ? user.name : data.name) : user.name} className="border border-gray-400 rounded d-inline mt-2   px-4 py-2 w-full" name="name" />
                </div>
                <div className="form-group mt-2">
                    <label className="text-gray-700">Email :</label>
                    <input onChange={handleChange} value={editMode ? (user.email ? user.email : data.email) : user.email}  className="border border-gray-400 rounded d-inline mt-2   px-4 py-2 w-full" type="email" name="email" />
                </div>
                <div className="form-group mt-2">
                    <label className="text-gray-700">Password :</label>
                    <input onChange={handleChange}   className="border border-gray-400 rounded d-inline mt-2   px-4 py-2 w-full" type="password" name="password" />
                </div>
                <div className="form-group mt-2">
                    <label className="text-gray-700">Role :</label>
                    <select onChange={handleChange} name="role_id" value={editMode ? (user.role_id ? user.role_id : data.role_id) : user.role_id}   className="border border-gray-400 rounded d-inline mt-2   px-4 py-2 w-full">
                        <option value="" default selected>-- Select Role --</option>
                        <option value="2">Staff</option>
                        <option value="1">Admin</option>
                    </select>
                   </div> 
                <div className="mt-5"> 
                    <button type="submit"  className="bg-green-500 shadow  hover:bg-green-700 text-white  py-2 px-2 rounded pull-right">Submit</button>
                </div>
            </form>
        </div>
      </div>
  );
}

Index.propTypes = {}

export default Index;

