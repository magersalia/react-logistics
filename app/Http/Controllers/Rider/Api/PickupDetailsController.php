<?php

namespace App\Http\Controllers\Rider\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PickupDetail;
use App\ParcelTimeline;
class PickupDetailsController extends Controller
{
    //


    public function details(Request $request){
        $pickup_detail = PickupDetail::whereOrderId($request->id)->first();
    }

    public function update(Request $request){
        $pickup_details = PickupDetail::find($request->id);
        $pickup_details->status = 2;
        $pickup_details->picked_up_at = \Carbon\Carbon::now();
        $pickup_details->save(); 

        $this->saveTimeLine($pickup_details->order_id,'Picked Up','The item has been successfully picked up on '.$pickup_details->address);
 
        // $this->sendDataToBentahan(6,$pickup_details->order->bentahan_order_id);

        return response()->json(['message' => 'OK','success'=>true]); 
    }
    public function sendDataToBentahan($status,$order_id){
        $param = 'api/order/'.$order_id.'/update';
        $data['status_id'] = $status;
        $apiData = ApiDataHelper::sendData($param,$data);
        return $apiData;
    }


    public function cancel(PickupDetail $pickup_detail, Request $request){  
        $pickup_detail->isCancelled = true;
        $pickup_detail->cancelled_at = \Carbon\Carbon::now();
        $pickup_detail->cancelled_reason = $request->reason;
        $pickup_detail->save();

        $this->saveTimeLine($pickup_detail->order_id,'Cancelled Pickuped',$request->reason);

        return response()->json(['message' => 'OK','success'=>true,'order_id'=>$pickup_detail->order_id]);  
    }

    public function saveTimeLine($order_id,$status,$description){

        $data = new ParcelTimeline();
        $data->order_id = $order_id;
        $data->status = $status;
        $data->description = $description;
        $data->save();

    }

}
