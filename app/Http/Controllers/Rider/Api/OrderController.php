<?php

namespace App\Http\Controllers\Rider\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\ParcelTimeline;
use App\Http\Helpers\ApiDataHelper; 
class OrderController extends Controller
{
    //

    public function getDataForDashboard(Request $request){
        $id = $request->id;
        $data['nav']['active'] = Order::whereRiderId($id)->onGoing()->count(); 
        $data['nav']['pending'] = Order::whereRiderId($id)->pending()->count(); 
        $data['nav']['completed'] = Order::whereRiderId($id)->completed()->count(); 
        $data['nav']['cancelled'] = Order::whereRiderId($id)->cancelled()->count(); 
        
        
        $orders = Order::with(['delivery_details','pickup_details','history','status'])->whereRiderId($id)->latest('updated_at'); 

        $data['nav']['all'] =$orders->count();
    
        // dd(request()->get('status') == "null");
        if (request()->has('status')) {
            if(request()->get('status') != "null"){ 
                $orders->where('status_id',request()->get('status'));   
            }
        }

        $data['orders'] = $orders->get();
        
        return response()->json(['message' => 'OK','data'=>$data]);  

    }

    
    public function details(Request $request){
        $order = Order::with(['delivery_details','pickup_details','history','status'])->find($request->id); 
        return response()->json(['message' => 'OK','data'=>$order]); 
    }


    public function sendDataToBentahan($status,$order_id){
        $param = 'api/order/'.$order_id.'/update';
        $data['status_id'] = $status;
        $apiData = ApiDataHelper::sendData($param,$data);
        return $apiData;
    }
    public function updateStatus(Request $request){
 
        $order = Order::find($request->id); 
        $now = \Carbon\Carbon::now();
        if($request->status == 9){
            $order->start_date = $now;
            $this->saveTimeLine($order->id,'Accepted','The order has been accepted by the delivery rider');
            // $this->sendDataToBentahan(5,$order->bentahan_order_id);
        }
        if($request->status == 11){
            $this->saveTimeLine($order->id,'Completed','The order has been successfully completed');
            $order->completed_at = $now;
            // $this->sendDataToBentahan(7,$order->bentahan_order_id);
        }
        if($request->status == 12){
            $order->cancelled_at = $now;
        }
        if($request->status == 10){
            $order->pickup_at = $now;
        }
        if($request->status == 5){
            $order->delivered_at = $now;
        }

        $order->status_id = $request->status;
        $order->save();

        return response()->json(['message' => 'OK']); 
    }

    
    public function saveTimeLine($order_id,$status,$description){

        $data = new ParcelTimeline();
        $data->order_id = $order_id;
        $data->status = $status;
        $data->description = $description;
        $data->save();

    }
}
