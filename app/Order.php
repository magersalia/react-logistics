<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{

    protected $fillable = ['order_id','tracking_no','size','weight','total','rider_id','bentahan_order_id'];
    protected $appends = ['duration','time_since'];
    public function status(){
        return $this->hasOne('App\OrderStatus','id','status_id')->withDefault();
    }

    public function rider(){
        return $this->belongsTo('App\User','rider_id','id')->withDefault();
    }
    
    public function scopeOnGoing($query){
        return $query->where('status_id',9);
    }

    public function scopePending($query){
        return $query->where('status_id',3);
    }

    public function scopeCompleted($query){
        return $query->where('status_id',11);
    }
    public function scopeCancelled($query){
        return $query->where('status_id',12);
    }

    public function delivery_details(){
        return $this->hasOne('App\DeliveryDetail')->withDefault();
    }
    public function pickup_details(){
        return $this->hasOne('App\PickupDetail')->withDefault();
    }

    public function history(){
        return $this->hasMany('App\ParcelTimeline');
    }

    public function getDurationAttribute(){
        $endTime = $this->status->name == 'Cancelled' ? $this->cancelled_at : $this->completed_at;
        $startTime = \Carbon\Carbon::parse($this->start_date); 
        $endTime = $endTime ? \Carbon\Carbon::parse($endTime) : \Carbon\Carbon::now();
        $time =  $startTime->diff($endTime)->format('%h Hours %i Minutes %s Seconds'); 
        $days =  $startTime->diffInDays($endTime); 
        return $days ? $days.' Day(s)' : '' . $time; 
    }

    public function getTimeSinceAttribute(){
        $endTime = $this->status->name == 'Cancelled' ? $this->cancelled_at : $this->completed_at;
        $startTime = \Carbon\Carbon::parse($this->created_at); 
        $endTime = $endTime ? \Carbon\Carbon::parse($endTime) : \Carbon\Carbon::now();
        $time =  $startTime->diff($endTime)->format('%h Hours %i Minutes %s Seconds'); 
        $days =  $startTime->diffInDays($endTime); 
        // return $days ? 'i have days' : 'no days'; 
        return $days ? $days.' Day(s) ago' :  $time; 
    }
}
