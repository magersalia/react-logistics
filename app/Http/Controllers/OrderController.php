<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class OrderController extends Controller
{
    //
    public function track(Request $request){
        $order_id = $request->order_id;
        $tracking_no = $request->tracking_no;
        $order = Order::with(['history'])->where('tracking_no',$tracking_no)->first(); 
       return response()->json(['success'=>true,'data'=>$order]);
    }
    
    // public function saveTimeLine($order_id,$status,$description){

    //     $data = new ParcelTimeline();
    //     $data->order_id = $order_id;
    //     $data->status = $status;
    //     $data->description = $description;
    //     $data->save();

    // }
}
