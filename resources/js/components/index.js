import React from 'react';
import ReactDOM from 'react-dom'; 
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"; 
import AdminInterface from './admin/index';
import RiderInterface from './rider/index';

function AdminApp () {
    return (    
        <AdminInterface />
    );
  }

  function RiderApp () {
    return (    
        <RiderInterface />
    );
  }
     
  export default user.role_id == 3 ? RiderApp : AdminApp
  
if (document.getElementById('app')) {
    ReactDOM.render(user.role_id == 3 ? <RiderApp/> : <AdminApp/>, document.getElementById('app'));
}
