import React, { useEffect, useState } from "react"
import ReactDOM from 'react-dom'; 
import { 
  useLocation 
} from "react-router-dom";
 

import TopBarProgress from "react-topbar-progress-indicator" 

TopBarProgress.config({
  barColors: {
    "0": "#10B981",
    "1.0": "#10B985"
  },
  shadowBlur: 5
});
 
export default function App() {
  const [progress, setProgress] = useState(false);
  const [prevLoc, setPrevLoc] = useState({});
  let location = useLocation();

  useEffect(() => {
    setPrevLoc(location);
    setProgress(true);
    window.scrollTo(0, 0); //Go to top on every page load
    }, [location]);
    
    useEffect(() => {
    setProgress(false);
    }, [prevLoc]);
 
  return (    
        <> 
          {progress && <TopBarProgress />} 
        </>  
  );
}
 