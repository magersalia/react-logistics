<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
 

    <!-- Fonts --> 

    <!-- Styles --> 
    <link rel="stylesheet" href="{{asset('/font/css/all.css')}}">

    <link href="{{asset('anchor/assets/css/main.css')}}" rel="stylesheet"/>
    <link href="{{asset('anchor/assets/css/vendor/aos.css')}}" rel="stylesheet"/> 
</head>
<body class="bg-light mb-5">
    <div id="app"> 
        @include('layouts.navbar') 
        
        {{-- <div class="container-fluid pt-4"> --}}
            
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
            @yield('content')
        {{-- </div> --}}
    </div>
    @stack('js')
    
    <!-- minimal footer -->
{{-- <footer class="bg-white pt-2 pb-2 fixed-bottom">
    <div class="container text-center">  © Copyright 2021 <br>Bentahan.ph  
    </div>
    </footer> --}}
<!-- Javascript -->
<script src="{{asset('anchor/assets/js/vendor/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/vendor/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/vendor/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('anchor/assets/js/functions.js')}}" type="text/javascript"></script>

</body>
</html>
