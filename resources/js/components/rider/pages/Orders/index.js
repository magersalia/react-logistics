import React,{useState,useEffect} from 'react';

 
import { NavLink, Link, useHistory,useLocation } from 'react-router-dom';

import DataTable from './DataTable';
import Spinner from '../../../elements/Spinner'
import { PlusIcon,CurrencyDollarIcon } from '@heroicons/react/solid'



import { ToastContainer, toast,Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Home (props) {

  let [orders,setOrders] = useState(null);
  let [loading,setLoading] = useState(false);

  const location = useLocation();
  const history = useHistory();
 
  const notify = (message) =>{
    toast.success(message, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined, 
      });
  };

  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

  const getData =()=>{
    let url = '/api/admin/orders';
    // setLoading(true);
    axios.get(url)
    .then(response => {  
        setOrders(response.data.data)
    // setLoading(false);

    })
    .catch(error => {
        // setisLoading(false)
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        }
        // Swal.fire("Something went wrong!", "", "error")
    });

    
  }


  
  useEffect(() => { 
    getData()
    
    if(location.state){

      if(location.state.withSuccess){
        notify('Data successfully saved')
    history.replace({ state: {} })

      }
    }

}, []);


const tableHeading = [{
    name:'Order ID'
  },{
    name:'Tracking No'
  },{
    name:'Destination'
  },{
    name:'Size'
  },{
    name:'Weight'
  },{
    name:'Total'
  },{
    name:'Created at'
  },{
    name:'Time since placed'
  },{
    name:'Assigned Rider'
  },{
    name:'Status'
  },{
    name:'Actions'
  } 
]; 
  return ( 
      <div>
          {
            
            loading ?
                <Spinner/>
              :
              '' 
          }   
          <ToastContainer position="top-right"/>

        <header className="bg-white overflow-hidden mb-3 w-full ">
        <div className=" py-4  grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500  mt-1">
          <CurrencyDollarIcon aria-hidden="true" className="inline h-6 w-6 pb-1 mr-2" />Orders List</h1>
        </div>
        <div className="text-right mt-1"> 
        <Link to="/admin/orders/create" className={`bg-green-500 hover:bg-green-700 text-white  py-2 px-2 rounded  shadow  text-right`}>
        Make New Order 
        </Link> 
        </div>
        </div>
      </header>

        <DataTable data={orders} toast={notify} reloadData={getData} heading={tableHeading} />
      
      </div>
  );
}

Home.propTypes = {}

export default Home;

