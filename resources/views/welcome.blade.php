@extends('layouts.frontend.home')
@section('content')
<!--------------------------------------
NAVBAR
--------------------------------------->
<nav class="topnav navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
<div class="container-fluid">
	<a class="navbar-brand" href="./index.html"><i class="fas fa-box-open text-success mr-2"></i><strong class="text-success">Bentahan</strong> Logistics   </a>
	<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="navbar-collapse collapse" id="navbarColor02" style="">
	 
		<ul class="navbar-nav ml-auto d-flex align-items-center">
			@auth
				
			<li class="nav-item"> 
				<a class="nav-link" href="{{url('/home')}}">Home</a>
				</a> 
			</li>
			@else 
			<li class="nav-item"> 
				<a class="nav-link" href="{{url('/login')}}">Login</a>
				</a> 
			</li>
			@endauth 
		</ul>
	</div>
</div>
</nav>
<!-- End Navbar -->
    
    
<!-------------------------------------
HEADER
--------------------------------------->
<div class="jumbotron jumbotron-xl jumbotron-fluid overlay overlay-blue" style="background-size:cover; background-image:url(https://media.istockphoto.com/photos/asian-postman-deliveryman-wearing-mask-carry-small-box-deliver-to-in-picture-id1217702116?k=20&m=1217702116&s=612x612&w=0&h=TlCp1L7Yn_sZ3P2JiDdKDboN0NvQi2rwsrAKmkc11RA=);">
    <div class="container-fluid text-white h-100">
        <div class="d-lg-flex align-items-center justify-content-between text-center pl-lg-5">
            <div class="col pt-4 pb-4">
                <h1 class="display-3">
					<i class="fas fa-box-open text-success mr-2"></i><br>
					A smart way of <strong>Shipping</strong> 

                </h1>
                <h5 class="font-weight-light mb-4">Track Your Parcel Now!</h5>
               
            </div>
            <div class="col align-self-bottom align-items-right text-right h-max-380 position-relative z-index-1">
                <img src="https://media.istockphoto.com/photos/asian-postman-deliveryman-wearing-mask-carry-small-box-deliver-to-in-picture-id1217702116?k=20&m=1217702116&s=612x612&w=0&h=TlCp1L7Yn_sZ3P2JiDdKDboN0NvQi2rwsrAKmkc11RA=" class="rounded shadow-lg img-fluid">
            </div>
        </div>
    </div>
</div> 
<!--- END HEADER --> 
<div class="container"  data-aos="fade-up">
	<form id="track-form">
		<div class="form-group row">
			<div class="col-lg-9 mt-2">
				<input type="text" class="form-control-lg w-100" name="tracking_no" placeholder="Tracking No">
			</div>
			{{-- <div class="col col-xs-12">
				<input type="text" class="form-control-lg w-100" name="order_id" placeholder="Order ID">
			</div> --}}
			<div class="col mt-2">
				<button class="btn btn-success  btn-lg shadow btn-block ">Track Parcel <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span></button>
			</div>
		</div> 
	</form>

</div>
 
<div class="container mt-5 mb-5 timeline col-lg-5 mx-auto" style="display: none"> 
	<h4>Parcel Timeline</h4>   
</div>
 

<div class="bg-white mt-5" >
	
<div class="container py-5"  data-aos="fade-up">
	
<div class="row">
    <div class="col-md-4">
        <div class="media">
            <div class="iconbox iconmedium rounded-circle text-info mr-4">
                <i class="fab fa-html5"></i>
            </div>
            <div class="media-body">
                <h5>Responsive</h5>
                <p class="text-muted">
                     Your website works on any device: desktop, tablet or mobile.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="media">
            <div class="iconbox iconmedium rounded-circle text-purple mr-4">
                <i class="fab fa-gulp"></i>
            </div>
            <div class="media-body">
                <h5>Gulp</h5>
                <p class="text-muted">
                     You can easily read, edit, and write your own code, or change everything.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="media">
            <div class="iconbox iconmedium rounded-circle text-info mr-4">
                <i class="fab fa-amazon"></i>
            </div>
            <div class="media-body">
                <h5>UI Kit</h5>
                <p class="text-muted">
                     There is a bunch of useful and necessary elements for developing your website.
                </p>
            </div>
        </div>
    </div>
</div>

</div>
</div>
<!--------------------------------------
FAQ
--------------------------------------->
<div class="bg-white">
	
<div class="container pt-5 pb-5 mb-5"  data-aos="fade-up">
	<div class="text-center pt-3 pb-4">
		<h2>Frequently Asked Questions</h2>
		<p class="text-muted">
			 Transparent answers, licensing and lore ipsum super funny.
		</p>
	</div>
	<div class="row gap-y justify-content-center">
		<div class="col-md-5">
			<h5>What is Anchor Bootstrap UI Kit?</h5>
			<p class="text-muted">
				 Anchor Bootstrap UI Kit is a set of polished components that you can use for building your own template. This page is built with it.
			</p>
		</div>
		<div class="col-md-5">
			<h5>Is it updated to Bootstrap?</h5>
			<p class="text-muted">
				 Yes, Anchor Bootstrap UI Kit is built with the latest Bootstrap version and will be updated whenever a new version is released.
			</p>
		</div>
		<div class="col-md-5">
			<h5>Can I use it for commercial projects?</h5>
			<p class="text-muted">
				 Absolutely! Use Anchor Bootstrap UI Kit for any personal and commercial projects.
			</p>
		</div>
		<div class="col-md-5">
			<h5>Is there a way to thank you?</h5>
			<p class="text-muted">
				 That is very nice of you. Sure, I would gladly accept a cup of coffee <a href="https://www.wowthemes.net/donate/">here</a>.
			</p>
		</div>
	</div>
</div>

</div>
<!-- End FAQ -->



@endsection

@push('js')
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script>
	$(function(){
		$('#track-form').on('submit',function(e){
			e.preventDefault()
		var tracking_no = $('input[name="tracking_no"]').val(),
			uiButton = $(this).find('button'),
			uiTimeLine = $('.timeline'); 
			// uiButton.html('Loading ...')
			uiTimeLine.html(`<div class="loader mx-auto"></div>`)
			uiTimeLine.remove('ul')
		$.ajax({ 
            type: 'GET',
            url: '/api/orders/tracker/',
            data: { 
                // 'tracking_no': 12345678
                'tracking_no': tracking_no
            },
            success: function (data) {
				var content = [];
				const order = data.data || {};
				// uiButton.html('Track Parcel')

				uiTimeLine.slideDown() 
				if(Object.keys(order).length >= 1){ 
					var timeline = order.history;
					uiTimeLine.html('<ul class="timeline"></ul>');
					timeline.map((item,key)=>
						content.push(`
						<li>
						<strong class="font-weight-bold">${item.status}</strong>
						<a href="#" class="float-right">${moment(item.created_at).format('MM/DD/Y, h:mm:ss a')}</a>
						<p class="mt-3">${item.description}</p>
					</li> `)
					)
					uiTimeLine.find('ul').html(content).fadeIn()
				}else{
						uiTimeLine.html(`<h2 class="text-center text-dark"><i class="fas fa-frown display-2 "></i><br>Ooops!<br> 
					<span class="font-weight-light">Parcel not found, please try again.</span>`)
				}
				
				
            },
            error: function (data, textStatus, errorThrown) {
                console.log("Error:");
                console.log(data);

            },
        });
	})
	
})

</script>
<script>
    AOS.init({
        duration: 700
    });
</script>
 
<!-- Disable animation on less than 1200px, change value if you like -->
<script>
AOS.init({
  disable: function () {
    var maxWidth = 1200;
    return window.innerWidth < maxWidth;
  }
});
</script>
@endpush