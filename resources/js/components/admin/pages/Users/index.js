import React,{useState,useEffect} from 'react'; 
 
 
import { NavLink, Link, useHistory,useLocation } from 'react-router-dom';

import DataTable from './DataTable';
import Spinner from '../../../elements/Spinner'
import { UsersIcon } from '@heroicons/react/solid'

import { ToastContainer, toast,Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Home (props) { 
  let [users,setUsers] = useState(null);
  let [loading,setLoading] = useState(false);
 

  const location = useLocation();
  const history = useHistory();

  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

  const getData =()=>{
    let url = '/api/admin/users';
    // setLoading(true);
    axios.get(url)
    .then(response => {  
        setUsers(response.data.data)
    // setLoading(false);

    })
    .catch(error => {
        setisLoading(false)
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        }
        // Swal.fire("Something went wrong!", "", "error")
    });
  }

  
  useEffect(() => { 
    getData() 
        
    if(location.state){ 
      if(location.state.withSuccess){ 
        notify('Data successfully saved')
        history.replace({ state: {} }) 
      }
    }
}, []);


const notify = (message) =>{
  toast.success(message, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined, 
    });
};


const tableHeading = [{
  name:'Name'
},{
  name:'Role'
},{
  name:'Created at'
},{
  name:'Status'
},{
  name:''
}
]; 
  return ( 
      <div>
          {
            
            loading ?
                <Spinner/>
              :
              '' 
          } 
 
          <ToastContainer position="top-right"/>
        <header className="bg-white overflow-hidden   rounded mb-4 ">
        <div className="mx-auto py-3  grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500 mt-2">
         <UsersIcon className="h-5 w-4 inline mr-1" aria-hidden="true" /> Users</h1>
        </div>
        <div className="text-right pt-3"> 
        <Link to="/admin/users/create" className={`bg-green-500 hover:bg-green-700 text-white    py-2 px-2 rounded pull-right shadow `}>
        Add User 
        </Link> 
        </div>
        </div>
      </header>

        <DataTable data={users} reloadData={getData} toast={notify} heading={tableHeading} />
      
      </div>
  );
}

Home.propTypes = {}

export default Home;

