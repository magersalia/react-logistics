import React,{useState,useEffect} from 'react';

 
import { NavLink, Link, useHistory,useLocation } from 'react-router-dom';

import DataTable from './DataTable';
import Spinner from '../../../elements/Spinner'
import { PlusIcon,CurrencyDollarIcon } from '@heroicons/react/solid'



import { ToastContainer, toast,Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Home (props) {

  let [orders,setOrders] = useState(null);
  let [loading,setLoading] = useState(false);

  const location = useLocation();
  const history = useHistory();
 
  const notify = (message) =>{
    toast.success(message, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined, 
      });
  };

  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

  const getData =(sort)=>{
    let url = '/api/admin/orders';
    // setLoading(true);
    console.log(sort)

    // let formData = new FormData();
    // formData.append(sort)
    axios.get(url,{
      params:sort
    })
    .then(response => {  
        setOrders(response.data.data)
    // setLoading(false);

    })
    .catch(error => {
        // setisLoading(false)
        if (error.response.status == 422) {
            // setFormErrors(error.response.data.errors)
        }
        // Swal.fire("Something went wrong!", "", "error")
    });

    
  }


  
  useEffect(() => { 
    let sortData = {};
    sortData['sort'] = 'newest'; 
    getData(sortData)
    
    if(location.state){

      if(location.state.withSuccess){
        notify('Data successfully saved')
        history.replace({ state: {} })

      }
    }

}, []);


const tableHeading = [{
    name:'Order Details'
  } ,{
    name:'Size'
  },{
    name:'Weight'
  },{
    name:'Total'
  },{
    name:'Destination'
  },{
    name:'Created at'
  }
  ,{
    name:'Assigned Rider'
  },{
    name:'Status'
  },{
    name:'Actions'
  } 
]; 

const onChangeSort = (event) =>{
  let inputData = {};
  inputData['sort'] = event.target.value; 
  getData(inputData);
}
  return ( 
      <div>
          {
            
            loading ?
                <Spinner/>
              :
              '' 
          }   
          <ToastContainer position="top-right"/>

        <header className="bg-white overflow-hidden mb-3 w-full ">
        <div className="mb-2 grid lg:grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500 ">
          Orders List</h1>
        </div>
        <div className="lg:text-right  mt-1 "> 
        <Link to="/admin/orders/create" className={`bg-green-500 hover:bg-green-700 text-white  py-1 px-2 rounded  shadow  text-right`}>
        Make New Order 
        </Link> 
        </div>
        </div>
      </header>
        <select onChange={onChangeSort} className="p-1 mb-3 border border-gray-300 text-gray-500 right rounded">
          <option default selected hidden value="">-- Sort By --</option>
          <option value="newest">Newest first</option>
          <option value="oldest">Oldest first</option>
        </select>
        <DataTable data={orders} toast={notify} reloadData={getData} heading={tableHeading} />
      
      </div>
  );
}

Home.propTypes = {}

export default Home;

