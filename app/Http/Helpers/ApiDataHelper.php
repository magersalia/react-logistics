<?php

namespace App\Http\Helpers; 

use Illuminate\Support\Facades\Http;
class ApiDataHelper {

    public static function sendData($param,$data){
        $url = config('app.bentahan_url').$param; 
        // dd($url);
        $response = Http::get($url,$data);  
        if($response->failed()){
            dd('Something went wrong in sending the data to api');
        }
        return true;
    }
}
