import React,{useState,useEffect} from 'react';

import { NavLink, Link, useHistory } from 'react-router-dom';
 
import CreateUpdateForm from './CreateUpdateForm';
import { PlusIcon,ArrowLeftIcon } from '@heroicons/react/solid'


function Index (props) {


  let [order,setOrder] = useState({});
  let [pickupDetails,setPickupDetails] = useState(null);
  let [deliveryDetails,setDeliveryDetails] = useState(null);

  let [loading,setLoading] = useState(false);
 
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
 

  const getData = () =>{
    let id = props.match.params.id,
        url = `/api/admin/orders/edit/${id}`;
        axios.get(url)
        .then(response => {  
          setOrder(response.data.data)     
          console.log(response.data.data)
          setDeliveryDetails(response.data.data.delivery_details)
          setPickupDetails(response.data.data.pickup_details)
        })
        .catch(error => { 
            if (error.response.status == 422) {
                // setFormErrors(error.response.data.errors)
            } 
        });
  }
  useEffect(() => { 
    getData() 
}, []);

 
  
  return ( 
      <div>
        {
          loading ?
            <div class="w-full h-full fixed block top-0 left-0 bg-gray-800 opacity-75">
              <span class="text-green-500 z-99 top-1/2 my-0 mx-auto block relative w-0 h-0"
              style={{top: '50%'}}>
              <div class="loader ease-linear rounded-full border-8 border-t-8 border-gray-200 h-10 w-10"></div>
              </span>
            </div>
            :
            ''
        }
        
        <header className="bg-white   overflow-hidden  mb-4 ">
        <div className="grid grid-cols-2 gap-4"> 
        <div> 
        <h1 className="font-bold text-green-500 mt-2">
        <Link to='/admin/orders'>
        <ArrowLeftIcon className="inline h-6 w-6 pb-1 mr-4" area-hidden="true" />
        </Link> 
          Update {order.order_id || ''}
          </h1>
        </div> 
        </div>
      </header> 

        <CreateUpdateForm data={order} id={order.id} pickupDetails={pickupDetails} deliveryDetails={deliveryDetails} editMode={true} />
      
      </div>
  );
}

Index.propTypes = {}

export default Index;
 


