<?php

namespace App\Http\Controllers\Rider\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DeliveryDetail;
use App\ParcelTimeline;

use App\Http\Helpers\ApiDataHelper; 
class DeliveryDetailsController extends Controller
{
    //
    public function update(Request $request){
        $delivery_details = DeliveryDetail::find($request->id);
        $delivery_details->status = 2;
        $delivery_details->delivered_at = \Carbon\Carbon::now();
        $delivery_details->save(); 

        $this->saveTimeLine($delivery_details->order_id,'Delivered','The item has been successfully delivered on '.$delivery_details->address);
      
        // dd($delivery_details->order_id);
        // $this->sendDataToBentahan(6,$delivery_details->order->bentahan_order_id);


        return response()->json(['message' => 'OK','success'=>true]);  
    }

    public function sendDataToBentahan($status,$order_id){
        $param = 'api/order/'.$order_id.'/update';
        $data['status_id'] = $status;
        $apiData = ApiDataHelper::sendData($param,$data);
        return $apiData;
    }

    public function saveTimeLine($order_id,$status,$description){

        $data = new ParcelTimeline();
        $data->order_id = $order_id;
        $data->status = $status;
        $data->description = $description;
        $data->save();

    }
    
    public function cancel(DeliveryDetail $delivery_detail, Request $request){ 
        $delivery_detail->isCancelled = true;
        $delivery_detail->cancelled_at = \Carbon\Carbon::now();
        $delivery_detail->cancelled_reason = $request->reason;
        $delivery_detail->save();

        $this->saveTimeLine($delivery_detail->order_id,'Cancelled Delivery',$request->reason);
        return response()->json(['message' => 'OK','success'=>true]);  
    }
}
